Projects
--------

This is an experimental plugin for Friendica wich adds a git repository web interface

### What works ###

- create a repository via web interface, with a name, description and  standard Friendica ACL
- optionally post a status message on repository creation
- comments to a repository via standard friendica comment system
- browse repository tree
- push and pull of the repo via standard git smarthhtp
- git pull respecting ACL, git push only for repo owner
- fork a conctact's repo in personal project space, also from remote server (public repos only)
- display files in browser with code hilight where supported
- download binary files
- create a pull requests from a clone (public repos only)
- comment to pull requests directly from your network page
- merge/close pull requests

### What could works some day ###

- federated issue tracking
- comment to issues directly from your network page
- close issues directly from.. ok you get it
- clone and pull requests for private repositories

### How to install ###
Clone the repo and install required libraries

    cd /your/friendica/addon/
    git clone https://kirgroup.com/projects/fabrixxm/friendica-projects.git projects
    cd projects
    ./composer.phar install

Copy config.php.dist as config.php and set the path where the repos are saved. Must be writeable by the webserver

### How to update ###

After pulling the changes from the repository, remember to update the
dependencies by running composer.

    cd /your/friendica/addon/projects
    git pull
    ./composer.phar install

