<?php
/**
 * Name: Personal Projects
 * Description: Project repository for Friendica
 * Version: 0.1
 * Author: Fabio <https://kirgroup.com/~fabrixxm>
 */

require __DIR__ . '/vendor/autoload.php';

use Kirgroup\FProjects\VCS;
use Kirgroup\FProjects\Git;

require_once 'include/acl_selectors.php';
require_once 'include/security.php';

require_once 'include/network.php';
require_once 'include/items.php';
require_once 'include/bbcode.php';
require_once 'include/bb2diaspora.php';
require_once 'include/Scrape.php';

require_once 'mod/item.php';

if (!is_file(__DIR__.'/config.php')) {
	echo "Projects: Missing config.php. Please copy <tt>config.php.dist</tt> to <tt>config.php</tt> and edit it as needed.";
	killme();
}
require_once __DIR__.'/config.php';

define("OBJECT_PULLREQUEST", "http://kirgroup.com/schema/1.0/pullrequest");


/* pull requests statuses */
// NEW is for pr on "upstream", SENT for pr on "downstream"
define("PROJECTS_PR_STATUS_NEW", 0);
define("PROJECTS_PR_STATUS_SENT", 1);
define("PROJECTS_PR_STATUS_CLOSED", 2);
define("PROJECTS_PR_STATUS_MERGED", 3);

define("PROJECTS_VERB_PR_CLOSE", "pr_close");
define("PROJECTS_VERB_PR_MERGE", "pr_merge");


/**
 * Prjects plugin class
 */
class Projects {
	private static $inst=Null;

	private $supported_vcs = [
		"git" => "\Kirgroup\FProjects\VCS\GIT"
	];

	private $route = [
		'/_ajax_/(?<user>[^/]+)/(?<slug>[a-zA-Z0-9_-]+)/diff/(?<ref1>[^/]+)/(?<ref2>[^/]+)' => "ajax_diff",

		'/(?<user>[^/]+)/(?<slug>[a-zA-Z0-9_-]+)/pulls/?' => "pr_list",
		'/(?<user>[^/]+)/(?<slug>[a-zA-Z0-9_-]+)/pull/new/(?<ref>[^/]+)' => "pr_startnew",
		'/(?<user>[^/]+)/(?<slug>[a-zA-Z0-9_-]+)/pull/(?<id>[^/]+)(?<tab>[/a-z]*)' => "pr_show",
		'/(?<user>[^/]+)/(?<slug>[a-zA-Z0-9_-]+)/fork' => "forkrepo",
		'/(?<user>[^/]+)/(?<slug>[a-zA-Z0-9_-]+)/blob/(?<hash>[a-z0-9]+)' => "blob",
		'/(?<user>[^/]+)/(?<slug>[a-zA-Z0-9_-]+)/commits/(?<ref>[^/]+)' => "commits",
		'/(?<user>[^/]+)/(?<slug>[a-zA-Z0-9_-]+)/(?<ref>[^/]+)/(?<path>.*)' => "tree",
		'/(?<user>[^/]+)/(?<slug>[a-zA-Z0-9_-]+)/(?<ref>[^/]+)' => "project",
		'/(?<user>[^/]+)/(?<slug>[a-zA-Z0-9_-]+)' => "project",
		'/fork' => "forkremoterepo",
		'/new' => "newrepo",
		'/del' => "delrepo",
		'/(?<user>.+)' => "index",
		'' => "list_public_projects",
	];

	private $a;
	private $vcs_inst = null;
	private $uid = null;
	private $user = null;
	private $project = null;
	private $view_func = null;
	private $view_args = null;

	public function __construct(&$a=null) {
		$this->a = &$a;
		foreach ($this->supported_vcs as $klass) {
			$r = call_user_func( "$klass::getServeRoute" );
			$this->route = array_merge([ $r => "serve" ], $this->route);
		}
	}

	/**
	 * Singleton: returns Projects instance
	 */
	public static function instance(&$a=null){
		if (is_null(self::$inst)) {
			self::$inst = new Projects($a);
		}
		return self::$inst;
	}

	public function is_owner() {
		return local_user() == $this->uid;
	}

	/**
	 * Plugin interface: init()
	 */
	public function init() {
		if (!is_dir(REPO_BASE_PATH)) {
			echo "REPO_BASE_PATH is not a directory!";
			killme();
		}

		$this->a->page['htmlhead'] .= '<link rel="stylesheet" type="text/css" href="./addon/projects/styles/style.css" media="all" />';
		$this->a->page['htmlhead'] .= '<link rel="stylesheet" type="text/css" href="./addon/projects/octicons/octicons.css" media="all" />';
		$this->a->page['htmlhead'] .= '<link rel="stylesheet" type="text/css" href="./addon/projects/vendor/razielanarki/hyperlight/vibrant-ink.css" media="all" />';
		$this->a->page['htmlhead'] .= '<script type="text/javascript" src="./addon/projects/js/projects.js" ></script>';
		$this->_route();
		if (!is_null($this->view_args) && x($this->view_args, "user")){

			if (x($this->view_args, "slug") ) {

				$user = $this->view_args['user'];
				$slug = $this->view_args['slug'];
				$_def_rev_args = ['user'=>$user,'slug'=>$slug];

				$this->load_user($user);
				$this->load_project($slug);

				/*
				 * project aside menu
				 */

				// count pullrequests
				$count_pr = 0;
				$r = q("SELECT count(guid) as tot FROM projects_pr as pr
							JOIN projects as p ON p.id = pr.pid
							WHERE p.name='%s' AND pr.status=%d and p.uid=%d",
							dbesc($slug),
							PROJECTS_PR_STATUS_NEW,
							intval($this->uid));
				if ($r!==false && count($r)>0) $count_pr = $r[0]['tot'];

				$page = "";
				if ($this->a->argc>3)
					$page = $this->a->argv[3];

				$vcs = $this->vcs($user, $slug);
				$remote = $vcs->getRemote();
				$repotype = (is_null($remote)? 'repo':'repo-clone');

				// main links (pull requests, issues, etc)
				$t = get_markup_template( "aside.tpl", "addon/projects/" );
				$o = replace_macros($t, array(
					'$baseurl' => $this->a->get_baseurl(),
					'$user' => $this->view_args['user'],
					'$slug' => $this->view_args['slug'],
					'$links' => [
						[ t('Code'), $this->route_reverse('project',$_def_rev_args), 'code', $page=='', null],
						[ t('Pull requests'), $this->route_reverse('pr_list',$_def_rev_args), 'git-pull-request', $page=='pulls'||$page=='pull', $count_pr],

					],
					'$cloneurl' => $this->repocloneurl($user, $slug, $this->project['is_public']),
					'$repotype' => $repotype
				));

				$this->a->page['aside'] .= $o;
			} else {
				profile_load($this->a, $this->view_args['user']);
			}
		}

	}

	/**
	 * RegExp based routing
	 */
	private function _route() {
		$this->view_func = null;
		$this->view_args = null;

		foreach($this->route as $path=>$func){
			$match = array();
			if (preg_match("|projects$path|", $this->a->cmd, $match)) {
				$args = [];
				foreach ($match as $k=>$v)
					if (is_string($k)) $args[$k]=$v;

				$this->view_func = [$this, $func];
				$this->view_args = $args;
				return;
			}
		}
	}

	/**
	 * Reverse url
	 */
	private function route_reverse($name, $args=[]) {
		$reversed_routes = array_reverse($this->route);
		foreach ($reversed_routes as $path=>$func) {
			if ($name == $func) {
				// check if we have same args as in path
				$m = [];
				preg_match_all("|\(\?\<(\w+)\>[^)]*\)|", $path, $m);

				// replace args
				if ($m[1] == array_keys($args)) {
					$path = preg_replace_callback(
						"|\(\?\<(\w+)\>[^)]*\)|",
						function ($match) use ($args) {
							if (count($match)>1 && x($args, $match[1])) {
								return $args[$match[1]];
							}
						},
						$path);
					$path = $this->a->get_baseurl()."/projects".$path;
					return $path;
				}
			}
		}
		return null;
	}

	/**
	 * Plugin interface: content()
	 */
	public function content() {
		if (is_null($this->view_func)) return $this->notfound();
		$o = call_user_func_array($this->view_func, $this->view_args);
		return profile_tabs($this->a, $this->is_owner(), $this->user). $o;
	}

	/**
	 * Plugin interface: post()
	 */
	public function post() {
		if (is_null($this->view_func)) return $this->notfound();

		$view_func = $this->view_func;
		$view_func[1] .= "_post";
		return call_user_func_array($view_func, $this->view_args);
	}

	public function pr_parse_object($str_object){
		$object = parse_xml_string($str_object, false);
		$object_link = parse_xml_string($object->link, false);
		if ($object_link===false) {
			$object_link = $object->link;
			list($head_url, $_refs) = explode("#", (string)$object->link);
		} else {
			list($head_url, $_refs) = explode("#", $object_link->attributes()->href);
		}
		list($head_ref, $_refs) = explode(":", $_refs);
		list($user, $slug, $base_ref) = explode("/", $_refs );

		return [$object, $object_link, $user, $slug, $head_url, $head_ref, $base_ref];
	}

	/**
	 * Plugin interface: hook "post_local_end"
	 */
	public function on_post_remote_end(&$a, &$item) {
		// this code come mostly from include/item.php:tag_deliver()
		logger($item, LOGGER_DEBUG);

		$uid = $item['uid'];
		logger("uid: ".$uid, LOGGER_DEBUG);

		//TODO: close and merge should be checked for sender contact.
		// it must be upstream contact

		// close pull request
		if ($item['verb']===PROJECTS_VERB_PR_CLOSE) {
			logger("Close pull request", LOGGER_NORMAL);
			$r = q("SELECT guid FROM item WHERE id=%d",
					intval($item['parent'])
				);
			if ($r===false || count($r)==0) {
				// delivery chain should have already checked for this... but...
				logger("Parent ".$item['parent']." not found. Reject", LOGGER_NORMAL);
				return;
			}
			q("UPDATE projects_pr SET status=%d, closed='%s' WHERE guid='%s'",
				PROJECTS_PR_STATUS_CLOSED,
				dbesc(datetime_convert()),
				dbesc($r[0]['guid'])
			);
			return;
		}

		// merge pull request
		if ($item['verb']===PROJECTS_VERB_PR_MERGE) {
			logger("Merge pull request", LOGGER_NORMAL);
			$r = q("SELECT guid FROM item WHERE id=%d",
					intval($item['parent'])
				);
			if ($r===false || count($r)==0) {
				// delivery chain should have already checked for this... but...
				logger("Parent ".$item['parent']." not found. Reject", LOGGER_NORMAL);
				return;
			}
			q("UPDATE projects_pr SET status=%d, closed='%s' WHERE guid='%s'",
				PROJECTS_PR_STATUS_MERGED,
				dbesc(datetime_convert()),
				dbesc($r[0]['guid'])
			);
			return;
		}

		if($item['id'] != $item['parent']) {
			logger(sprintf("Only top level post from here. id:%s , parent:%s", $item['id'], $item['parent']), LOGGER_DEBUG);
			return;
		}

		// if object type == OBJECT_PULLREQUEST
		// parse object, save in db, change post to
		// wall-to-wall if remote
		if ($item['object-type']!== OBJECT_PULLREQUEST) return;

		// new pull request
		logger("pull request!", LOGGER_DEBUG);

		list($object, $object_link, $user, $slug, $head_url, $head_ref, $base_ref) = $this->pr_parse_object($item['object']);
		logger(print_r([$object, $object_link, $user, $slug, $head_url, $head_ref, $base_ref], true), LOGGER_DEBUG);

		if (!$this->load_user($user)) {
			logger("Cannot load user '$user'", LOGGER_NORMAL);
			return;
		}
		if ($uid != $this->uid) {
			logger("Recipient UID ($uid) and pull request user (".$this->uid.") don't match. reject", LOGGER_NORMAL);
			return;
		}

		if (!$this->load_project($slug)) {
			logger("Project '$slug' for user '$user' not found. reject", LOGGER_NORMAL);
			return;
		}

		// check if we have already processed this PR
		$r = q("SELECT guid FROM projects_pr WHERE guid='%s' AND pid=%d",
				dbesc($item['guid']),
				intval($this->project['id'])
			);
		logger("We have already processed this PR", LOGGER_DEBUG);
		if (count($r)>0) return;


		$rins = q("INSERT INTO projects_pr
			(guid, pid, status, head_url, head_ref, base_url, base_ref) VALUES
			('%s', %d, %d, '%s', '%s', '%s', '%s');",
			dbesc($item['guid']),
			intval($this->project['id']),
			PROJECTS_PR_STATUS_NEW,
			dbesc($head_url),
			dbesc($head_ref),
			dbesc($this->project['url']),
			dbesc($base_ref)
		);
		if ($rins===false) {
			logger("Error inserting PR!!!!!!!!!!");
			return;
		}

		// now change this copy of the post to a wall-to-wall head message and deliver

		$c = q("select name, url, thumb from contact where self = 1 and uid = %d limit 1",
			intval($this->uid)
		);
		if(! count($c)) {
			logger("No contact for uid ".$this->uid .". reject", LOGGER_NORMAL);
			return;
		}
		// also reset all the privacy bits to the project default permissions

		$private = ($this->project['allow_cid'] || $this->project['allow_gid'] || $this->project['deny_cid'] || $this->project['deny_gid']) ? 1 : 0;

		$plink = $this->route_reverse("pr_show",['user'=>$user, 'slug'=>$slug, 'id'=>$item['guid'], 'tab'=>'']);

		$r = q("update item set wall = 1, origin = 1, type='project.pullrequest',
			`owner-name` = '%s', `owner-link` = '%s', `owner-avatar` = '%s',
			`private` = %d, `allow_cid` = '%s', `allow_gid` = '%s',
			`deny_cid` = '%s', `deny_gid` = '%s', plink = '%s'
			 where id = %d",
			dbesc($c[0]['name']),
			dbesc($c[0]['url']),
			dbesc($c[0]['thumb']),
			intval($private),
			dbesc($this->project['allow_cid']),
			dbesc($this->project['allow_gid']),
			dbesc($this->project['deny_cid']),
			dbesc($this->project['deny_gid']),
			dbesc($plink),
			intval($item['id'])
		);

		update_thread($item['id']);

		proc_run('php','include/notifier.php','tgroup',$item_id);

	}

	/**
	 * Search for user with nickname $nick
	 * sets $this->user and $this->uid
	 * returns true or false
	 */
	private function load_user($nick) {
		if ($this->user == $nick) return true;

		$r = q("SELECT uid FROM user WHERE nickname='%s'", dbesc($nick));
		if ($r!==false and count($r)){
			$this->user = $nick;
			$this->uid = intval($r[0]['uid']);
			return true;
		} else {
			$this->user=Null;
			$this->uid=Null;
			return false;
		}
	}

	private function load_project($slug) {
		$perm_sql = permissions_sql($this->uid);
		$r = q("SELECT * FROM projects WHERE uid=%d AND name='%s' %s",
				$this->uid,
				dbesc($slug),
				$perm_sql);
		if ($r===false || count($r)==0) return false;
		$vcs = $this->vcs($this->user, $slug);
		$this->project = $r[0];
		$this->project['url'] = $this->a->get_baseurl()."/projects/".$vcs->getClonePath();
		$this->project['path'] = $vcs->getRepoPath();

		$this->project['description'] = $vcs->getDescription();

		$this->project['is_public'] = ($r[0]['allow-cid'].$r[0]['allow-gid'].$r[0]['deny-cid'].$r[0]['deny-gid']) == "";

		return true;
	}

	private function get_user_record() {

		$r = q("SELECT `contact`.*, `user`.`nickname`
				FROM `contact` LEFT JOIN `user` ON `user`.`uid` = `contact`.`uid`
				WHERE `user`.`uid` = %d AND `self` = 1 LIMIT 1",
			intval($this->uid)
		);

		if(! count($r)) {
			logger('unable to locate contact record for project owner. uid=' . $this->uid);
			killme();
		}
		return $r[0];
	}

	private function commit_author_details($commit) {
		if (isset($this->_commit_author[$commit['email']])) {
			return $this->_commit_author[$commit['email']];
		}


		// is some contacts? (check from email)
		list($n, $s) = explode("@", $commit['email']);
		$profile = "http://$s/profile/$n";
		$r = q("SELECT `contact`.*
				FROM `contact`
				WHERE `contact`.`uid` = %d AND `contact`.`nurl` = '%s' LIMIT 1",
			intval($this->uid),
			dbesc($profile)
		);
		if (count($r)>0) {
			$this->_commit_author[$commit['email']] = $r[0];
			return $this->_commit_author[$commit['email']];
		}


		// we don't know him...
		$this->_commit_author[$commit['email']] = array(
			'name' => $commit['name'],
			'micro' => "./images/person-48.jpg"
		);
		return $this->_commit_author[$commit['email']];

	}

	/**
	 * returns "Not found" page with 404 header
	 *
	 * with $simple = true, echo a simple text and die
	 */
	private function notfound($simple=false) {
		header($_SERVER["SERVER_PROTOCOL"] . ' 404 ' . t('Not Found'));

		if ($simple===true) { echo t('Not found.' ); killme(); }

		$tpl = get_markup_template("404.tpl");
		return replace_macros($tpl, array(
			'$message' =>  t('Page not found.' )
		));
	}



	/**
	 * find clone url
	 * the clone url is different for:
	 * - public repo: plain project url
	 * - the owner : add owner username
	 * - allowed contact on private repo: add contact's urlencode("username@server") as username
	 */
	private function repocloneurl($user, $slug, $is_public) {
		$vcs = $this->vcs($user, $slug);
		$cloneurl = "/projects/". $vcs->getClonePath();
		$baseurl = explode("/",$this->a->get_baseurl());

		if ($this->is_owner()) {
				// the owner
				$baseurl[2] = $user."@".$baseurl[2];
				$cloneurl = implode("/", $baseurl) . $cloneurl;
		} else {
			if ($is_public) {
				$cloneurl = $this->a->get_baseurl() . $cloneurl;
			} else {
				if (remote_user()) {
					$r = $this->a->contact;
					$url = explode("/",$r['url']);
					$baseurl[2] = $r['nick']."%40".$url[2]."@".$baseurl[2];
					$cloneurl = implode("/", $baseurl) . $cloneurl;
				} else {
					$cloneurl = null;
				}
			}
		}
		return $cloneurl;
	}

	/**
	 * returns a vcs instance
	 */
	private function vcs($user, $slug){
		if (!is_null($this->vcs_inst)) return $this->vcs_inst;

		$r = q("SELECT projects.type FROM projects, user WHERE projects.uid=user.uid and user.nickname='%s' and projects.name='%s'",
				$user,
				$slug);

		if ($r===false || !count($r)) return false;
		$type = $r[0]['type'];

		if (!x($this->supported_vcs, $type)) return false;
		$klass = $this->supported_vcs[$type];

		try {
			$vcs = new $klass($this, $user, $slug);
		} catch ( VCS\RepoNotFoundException $e ) {
			return false;
		}

		$this->vcs_inst = $vcs;

		return $vcs;
	}


	/**
	 * Network!
	 */
	private function event_new_repo($slug, $visible){
		$owner_record = $this->get_user_record();
		if (!$this->load_project($slug)) return;

		$body = sprintf(
			"%s created new project '[url=%s]%s[/url]'\n\n[quote]%s[/quote]",
				$owner_record['name'],
				$this->a->get_baseurl()."/projects/".$this->user."/".$this->project['name'],
				$this->project['name'],
				$this->project['description']
		);
		$this->event($slug, $visible, $body);
	}

	private function event_fork_repo($slug, $remote, $visible) {
		$owner_record = $this->get_user_record();
		if (!$this->load_project($slug)) return;

		$body = sprintf(
			"%s forked '[url=%s]%s[/url]' from [url=%s]%s[/url]\n\n[quote]%s[/quote]",
				$owner_record['name'],
				$this->a->get_baseurl()."/projects/".$this->user."/".$this->project['name'],
				$this->project['name'],
				$remote, $remote,
				$this->project['description']
		);


		$this->event($slug, $visible, $body);
	}

	private function event($slug, $visible, $body) {
		$uri = item_new_uri($this->a->get_hostname(), $this->uid);

		if (!$this->load_project($slug)) return;

		$owner_record = $this->get_user_record();

		//$repopath = $this->repopath($this->user, $slug);
		//$description = "";
		//if (is_file($repopath."/description")) {
			//$description = file_get_contents($repopath."/description");
		//}

		// Create item container
		$arr = array();
		$arr['uid'] = $this->uid;
		$arr['uri'] = $uri;
		$arr['parent-uri'] = $uri;
		$arr['type'] = 'project';
		$arr['wall'] = 1;
		$arr['resource-id'] = $this->project['id'];
		$arr['contact-id'] = $owner_record['id'];
		$arr['owner-name'] = $owner_record['name'];
		$arr['owner-link'] = $owner_record['url'];
		$arr['owner-avatar'] = $owner_record['thumb'];
		$arr['author-name'] = $owner_record['name'];
		$arr['author-link'] = $owner_record['url'];
		$arr['author-avatar'] = $owner_record['thumb'];
		$arr['title'] = '';
		$arr['allow_cid'] = $this->project['allow_cid'];
		$arr['allow_gid'] = $this->project['allow_gid'];
		$arr['deny_cid'] = $this->project['deny_cid'];
		$arr['deny_gid'] = $this->project['deny_gid'];
		$arr['last-child'] = 1;
		$arr['visible'] = $visible;
		$arr['origin'] = 1;
		$arr['body'] = $body;
		#var_dump($arr); killme();
		$item_id = item_store($arr);


		if($visible)
			proc_run('php', "include/notifier.php", 'wall-new', $item_id);

	}

	private function get_comments($item_id) {
		$sql_extra = permissions_sql($this->uid);

		$r = q("SELECT `item`.*, `item`.`id` AS `item_id`,  `item`.`network` AS `item_network`,
			`contact`.`name`, `contact`.`photo`, `contact`.`url`, `contact`.`rel`,
			`contact`.`network`, `contact`.`thumb`, `contact`.`self`, `contact`.`writable`,
			`contact`.`id` AS `cid`, `contact`.`uid` AS `contact-uid`
			FROM `item` INNER JOIN `contact` ON `contact`.`id` = `item`.`contact-id`
			AND `contact`.`blocked` = 0 AND `contact`.`pending` = 0
			WHERE `item`.`uid` = %d AND `item`.`visible` = 1 AND `item`.`deleted` = 0
			and `item`.`moderated` = 0
			AND `item`.`parent` = (SELECT `parent` FROM `item` WHERE (`id` = '%s' OR `uri` = '%s')
			AND uid = %d)
			$sql_extra
			ORDER BY `parent` DESC, `gravity` ASC, `id` ASC",
			intval($this->uid),
			dbesc($item_id),
			dbesc($item_id),
			intval($this->uid)
		);


		/*
		if((local_user()) && (local_user() == $this->a->profile['uid'])) {
			q("UPDATE `item` SET `unseen` = 0
				WHERE `parent` = %d AND `unseen` = 1",
				intval($r[0]['parent'])
			);
		}
		*/

		$items = conv_sort($r,"`commented`");

		$o = conversation($this->a,$items,'display', false);

		return $o;
	}

	/**
	 * List all public repos page
	 */
	private function list_public_projects() {
		$list = q("SELECT p.*, u.nickname, u.username, c.micro FROM projects as p
					JOIN user as u ON p.uid=u.uid
					JOIN contact as c ON c.uid=u.uid
					WHERE
					c.self=1
					AND p.allow_cid='' AND p.allow_gid='' AND p.deny_cid='' AND p.deny_cid=''
					AND u.blocked=0 AND account_removed=0 AND account_expired=0
					ORDER BY u.username");

		$repos = []; $nname = "";
		if ($list !== false) {
			foreach ( $list as $repo ) {
				if ($nname!=$repo['nickname']){
					$nname=$repo['nickname'];
					$repos[$nname] = [];
				}
				if (!x($this->supported_vcs, $repo['type'])) continue;
				$klass = $this->supported_vcs[$repo['type']];
				$vcs = new $klass($this, $nname, $repo['name']);
				$repo['description'] = $vcs->getDescription();
				$repo['remote'] = $vcs->getRemote();
				$repos[$nname][] = $repo;
			}
		}

		$t = get_markup_template( "list_public_projects.tpl", "addon/projects/" );
		$o = replace_macros($t, array(
			'$baseurl' => $this->a->get_baseurl(),
			'$repos' => $repos,
		));
		return $o;
	}

	/**
	 * Index page
	 */
	private function index($user) {
		if (!$this->load_user($user)) return $this->notfound();

		$perm_sql = permissions_sql($this->uid);

		$list = q("SELECT * FROM projects WHERE uid=%d %s", $this->uid, $perm_sql);

		$repos = [];
		if ($list !== false) {
			foreach ( $list as $repo ) {
				if (!x($this->supported_vcs, $repo['type'])) continue;
				$klass = $this->supported_vcs[$repo['type']];
				$vcs = new $klass($this, $user, $repo['name']);
				$repo['description'] = $vcs->getDescription();
				$repo['remote'] = $vcs->getRemote();
				$repos [] = $repo;
			}
		}

		$t = get_markup_template( "index.tpl", "addon/projects/" );
		$o = replace_macros($t, array(
			'$user' => $user,
			'$repos' => $repos,
			'$landlord' => $this->is_owner()
		));
		return $o;
	}

	/**
	 * Add new repo page
	 */
	private function newrepo() {
		if (!local_user()) return $this->notfound();

		$rname = (x($_POST,"newrepo"))?$_POST["newrepo"]:"";
		$description = (x($_POST,"description"))?$_POST["description"]:"";
		$type = (x($_POST,"type"))?$_POST["type"]:"git";
		$nopost = x($_POST,"nopost");

		$types = array_keys($this->supported_vcs);
		$types = array_combine($types, $types);

		$t = get_markup_template( "newrepo.tpl", "addon/projects/" );
		$o = replace_macros($t, array(
			'$posturl' => $this->route_reverse('newrepo'),
			'$newrepo' => ['newrepo', t('Project name') , $rname , t('Only a-z, A-Z, 0-9, -, _')],
			'$type' => ['type', t('Type'), $type, '', $types ],
			'$description' => ['description', t('Description') , $description , ''],
			'$aclselect' => populate_acl($this->a->user,false),
			'$nopost' => ['nopost', t('Do not show a status post for this project'), $nopost],
			'$form_security_token' => get_form_security_token("newrepo")

		));
		return $o;
	}

	/**
	 * create a new repo
	 * empty or cloning a remote url
	 */
	private function newrepo_post() {
		if (!local_user()) return $this->notfound();

		check_form_security_token_redirectOnErr('/projects/new', 'newrepo');

		$user = $this->a->user['nickname'];
		if (!$this->load_user($user)) return $this->notfound();

		$description = (x($_POST,"description"))?$_POST["description"]:"";
		$type = (x($_POST,"type"))?$_POST["type"]:"git";
		$nopost = x($_POST,"nopost");

		if (!x($_POST,'newrepo')) {
			notice(t("Missing project name"));
			return;
		}
		$rname = $_POST["newrepo"];

		if (preg_match("|[^a-zA-Z0-9-_]|", $rname)) {
			notice(t("Invalid character in project name"));
			return;
		}

		if (!x($this->supported_vcs, $type)) {
			notice(t("Invalid project type"));
			return;
		}

		$klass = $this->supported_vcs[$type];
		$vcs = new $klass($this, $user, $rname);

		$repo_path = $vcs->getRepoPath();

		if (is_dir($repo_path)) {
			notice(sprintf(t("Project '%s' already exists"), $rname));
			return;
		}

		$str_group_allow   = perms2str($_POST['group_allow']);
		$str_contact_allow = perms2str($_POST['contact_allow']);
		$str_group_deny    = perms2str($_POST['group_deny']);
		$str_contact_deny  = perms2str($_POST['contact_deny']);

		$userdir =  REPO_BASE_PATH."/".$user;
		if (!is_dir($userdir)) mkdir($userdir, 0777, true);


		$remote_url=null;
		if (x($_REQUEST,"r")) {
			$r = $_REQUEST["r"];
			$tokens = array_map('base64url_decode', explode(".", $r));
			if (count($tokens)==3) $remote_url=$tokens[1];
			if (count($tokens)==2) $remote_url=$tokens[0];
			$vcs->clone_remote($remote_url);

		} else {
			$vcs->init();
		}

		q("INSERT INTO projects (uid, name, allow_cid, allow_gid, deny_cid, deny_gid)
			VALUE (%d, '%s', '%s', '%s', '%s', '%s')",
			local_user(), dbesc($rname), $str_contact_allow, $str_group_allow, $str_contact_deny, $str_group_deny);

		$vcs->setDescription($description);

		if (x($_REQUEST,"r")) {
			$this->event_fork_repo($rname, $remote_url, !$nopost);
		} else {
			$this->event_new_repo($rname, !$nopost);
		}
		goaway($this->route_reverse('index', ['user'=>$user]));

	}


	/**
	 * Delete repo page
	 *
	 * Responds only to POST requests from js code
	 */
	private function delrepo(){
		return $this->notfound();
	}

	private function delrepo_post() {
		ini_set('html_errors', 0);
		if (!local_user()) { echo "noauth"; killme(); }
		if (!x($_POST,'slug')) { echo "invalidparam"; killme(); }
		$slug = $_POST['slug'];
		$userid = local_user();

		$r = q("SELECT id FROM projects WHERE name='%s' and uid=%d",
				dbesc($slug),
				intval($userid)
		);


		if (count($r)) {
			$vcs = $this->vcs($this->a->user['nickname'], $slug);
			if ($vcs===false) { echo "cannotcreatevcsobject"; killme(); }
			$rd = $vcs->delete();
			if ($rd) {
				q("DELETE FROM projects WHERE id=%d", $r[0]['id']);
				echo "ok"; killme();
			}
			echo "cannotdeletefolder"; killme();
		}

		echo "invalidprojectforuser"; killme();
	}

	/**
	 * Fork repo page
	 */
	private function forkrepo($user, $slug) { return $this->notfound(); }

	private function forkrepo_post($user, $slug) {
		if (!$this->load_user($user)) return $this->notfound();

		$perm_sql = permissions_sql($this->uid);
		$repo = q("SELECT id, uid FROM projects WHERE uid=%d AND name='%s' %s", $this->uid, dbesc($slug), $perm_sql);
		if ($repo===false || count($repo)==0) return $this->notfound();
		$repo = $repo[0];

		$is_public = ($repo['allow-cid'].$repo['allow-gid'].$repo['deny-cid'].$repo['deny-gid']) == "";

		$vcs = $this->vcs($user, $slug);

		// get project description
		$description = $vcs->getDescription();

		$param = "r=";
		$param .= base64url_encode($vcs->getType());
		$param .= ".".base64url_encode($this->repocloneurl($user, $slug, $is_public));
		$param .= ".".base64url_encode($description);

		if (local_user() && !$this->is_owner()) {
			// local user but not owner
			$redirect = $this->route_reverse('forkremoterepo');
		}
		elseif (remote_user() && $this->a->contact['network']=='dfrn') {
			// authenticated remote user
			$redirect = $this->a->contact['url'];
			$redirect = preg_replace("|profile/.*|", "projects/fork", $redirect );
		}
		else {
			// unauthenticated user
			return "I don't know who you are.";
		}

		goaway($redirect."?".$param);
		killme();
	}

	/**
	 * Fork remote repository page
	 */
	private function forkremoterepo(){
		if (!x($_REQUEST,'r')) return $this->notfound();

		if (!local_user()) {
			notice( t('Permission denied.') . EOL);
			return;
		}


		$r = (x($_REQUEST,"r"))?$_REQUEST["r"]:"";

		$tokens = array_map('base64url_decode', explode(".", $r));
		if (count($tokens)==3) {
			list($type, $remote_url, $description) = $tokens;
		} else if (count($token)==2) {
			$type="git";
			list($remote_url, $description) = $tokens;
		} else {
			return "Invalid request.";
		}
		if (strstr($remote_url,"%40")) {
			return "We can't fork private repos yet...";
		}

		if (!x($this->supported_vcs, $type)) {
			return "This server don't supports '$type' repositories.";
		}

		$klass = $this->supported_vcs[$type];
		$slug = call_user_func($klass."::getSlugFromRemoteUrl", $remote_url);
		$nopost = x($_POST,"nopost");


		$t = get_markup_template( "clonerepo.tpl", "addon/projects/" );
		$o = replace_macros($t, array(
			'$posturl' => $this->route_reverse('newrepo'),
			'$r' => $r,
			'$remote_url' => $remote_url,
			'$newrepo' => $slug,
			'$description' => $description,
			'$type' => $type,
			'$aclselect' => populate_acl($this->a->user,false),
			'$nopost' => ['nopost', t('Do not show a status post for this project'), $nopost],
			'$form_security_token' => get_form_security_token("newrepo")
		));
		return $o;

	}


	/**
	 * markdown to html, with support for in-repo relative links
	 */
	private function _md2html($user, $slug, $ref, $str){
		$str = htmlspecialchars($str);
		$bbcode = diaspora2bb($str);

		function __local_link($user, $slug, $ref) {
			return function ($match) use ($user, $slug, $ref) {
				$a = get_app();
				if (substr($match[2],0,4)=='http') return $match[0];
				if ($match[1]=='[img]') {
					return sprintf('&min;img src="%s"&mag;',
						$a->get_baseurl()."/projects/$user/$slug/blob/$ref/".$match[2]
					);
				} else {
					return sprintf('&min;a href="%s"&mag;%s&min;/a&mag;',
						$a->get_baseurl()."/projects/$user/$slug/$ref/".$match[1],
						$match[2]
					);
				}
				#var_dump($user, $slug, $ref, $match); killme();
			};

			#'&min;a href="${1}"&mag;${2}&min;/a&mag;'
			#'&min;img src="${2}"&mag;'
		}

		$bbcode = preg_replace_callback("|\[url=([^\]]+)\]([^\]]*)\[/url\]|", __local_link($user, $slug, $ref), $bbcode);
		$bbcode = preg_replace_callback("|(\[img\])([^\]]*)\[/img\]|", __local_link($user, $slug, $ref), $bbcode);
		$str =  bbcode($bbcode, false, false);
		$str = str_replace(['&amp;min;','&amp;mag;'],['<','>'], $str);
		return $str;
	}

	/**
	 * Project info page
	 */
	private function project($user, $slug, $ref='master') {
		if (!$this->load_user($user)) return $this->notfound();

		$perm_sql = permissions_sql($this->uid);
		$repo = q("SELECT id, uid FROM projects WHERE uid=%d AND name='%s' %s", $this->uid, dbesc($slug), $perm_sql);
		if ($repo===false || count($repo)==0) return $this->notfound();
		$repo = $repo[0];

		$public = ($repo['allow-cid'].$repo['allow-gid'].$repo['deny-cid'].$repo['deny-gid']) == "";

		$vcs = $this->vcs($user, $slug);
		if ($vcs===false) return $this->notfound();

		// get projects comments and likes

		$item_id = q("SELECT id FROM item WHERE uid=%d AND type='project' AND `resource-id`='%s'",
			intval($this->uid),
			$repo['id']
		);
		$like_count = 0;
		$dislike_count = 0;
		$comments_html = "";
		if ($item_id!==false && count($item_id)>0) {
			$item_id = $item_id[0]['id'];

			$comments_html = $this->get_comments($item_id);

			$like_count = q("SELECT count(id) as c FROM item WHERE uid=%d AND verb='%s' AND parent=%d",
				intval($this->uid),
				dbesc(ACTIVITY_LIKE),
				intval($item_id)
			);
			$like_count = $like_count[0]['c'];

			$dislike_count = q("SELECT count(id) as c FROM item WHERE uid=%d AND verb='%s' AND parent=%d",
				intval($this->uid),
				dbesc(ACTIVITY_DISLIKE),
				intval($item_id)
			);
			$dislike_count = $dislike_count[0]['c'];
		}

		// get project description
		$description = $vcs->getDescription();

		// fork url
		$forkurl = $this->route_reverse('forkrepo', ['user'=>$user,'slug'=>$slug]);
		if ($this->is_owner() || (!$public && !remote_user())) $forkurl=null;

		// clone url
		$cloneurl = $this->repocloneurl($user, $slug, $public);

		// origin remote
		$remote = $vcs->getRemote();
		if (is_null($remote)) $remote = 'new';

		// empty project
		if (count($vcs->getBranches())==0) {
			$t = get_markup_template( "project_empty.tpl", "addon/projects/" );
			$o = replace_macros($t, array(
				'$ref' => $ref,
				'$slug' => $slug,
				'$user' => $user,
				'$like_count' => $like_count,
				'$dislike_count' => $dislike_count,
				'$branches' => 0,
				'$commits' => 0,
				'$comments_html' => $comments_html,
				'$description' => $description,
				'$cloneurl' => $cloneurl,
				'$forkurl' => $forkurl,
				'$remote' => $remote,
				'$isowner' => $this->is_owner()
			));
			return $o;

		}

		if (!array_key_exists($ref, $vcs->getBranches())) return $this->notfound();

		// search for readme
		$a_readme = $vcs->getReadme($ref);
		if (is_null($a_readme)) {
			$readme="";
		} elseif ( $a_readme['name']=="readme.md") {
			$readme = $code = $this->_md2html($user, $slug, $ref, $a_readme['blob']);
		} else {
			$readme = $a_readme['blob'];
		}

		$lastcommit = $vcs->getLog($ref)[0];
		$lastcommit['details'] = $this->commit_author_details($lastcommit);


		// custom git command "refcount": `git ref-list --count <ref>`;
		$gitrevcount = $vcs->getRevCount($ref);

		$t = get_markup_template( "project.tpl", "addon/projects/" );
		$o = replace_macros($t, array(
			'$slug' => $slug,
			'$user' => $user,
			'$ref' => $ref,
			'$readme' => $readme,
			'$lastcommit' => $lastcommit,
			'$branches' => count($vcs->getBranches()),
			'$commits' => $gitrevcount,
			'$like_count' => $like_count,
			'$dislike_count' => $dislike_count,
			'$breadcrumb' => $this->_breadcrumbs($vcs, $ref, ""),
			'$tree' => $this->_tree($vcs, $ref , ""),
			'$comments_html' => $comments_html,
			'$description' => $description,
			'$cloneurl' => $cloneurl,
			'$forkurl' => $forkurl,
			'$remote' => $remote,
			'$isowner' => $this->is_owner()
		));
		return $o;

	}

	private function _branchselector(&$vcs, $ref, $link_tpl) {
		$branches = $vcs->getBranches();
		// this is a silly name: $ref is not a branch name
		// note: this will be true also if $ref if the commit on HEAD of branch
		$outbranch = count($branches) && !isset($branches[$ref]);


		// this is not quite right. it assumes that every link we want
		// are under 'project' route
		$link_tpl = $this->route_reverse('project', [
			'user'=>$vcs->user,
			'slug'=>$vcs->slug
		]) . "/" . $link_tpl;

		$sel_branch=array();
		$sel_branch_current = sprintf($link_tpl, $ref);

		if ($outbranch) {
			$sel_branch[$sel_branch_current] = substr($ref,0,10);
		}

		foreach (array_keys($branches) as $k){
			$sel_branch[sprintf($link_tpl, $k)]=$k;
		}

		$t = get_markup_template( "_branch_select.tpl", "addon/projects/" );
		return replace_macros($t, array(
			'$field' => array("sel_branch","", $sel_branch_current, "" , $sel_branch)
		));
	}

	private function _breadcrumbs(&$vcs, $ref, $path, $is_file=false) {
		$sel_branch = $this->_branchselector($vcs, $ref, "%s/$path");

		$bc_link = $this->route_reverse('project',[
			'user' => $vcs->user,
			'slug' => $vcs->slug,
			'ref' => $ref
		]);

		$breadcrumb = array(
			$vcs->slug => $bc_link
		);
		if (trim($path, "/")!="") {
			foreach(explode("/", trim($path, "/")) as $p) {
				$bc_link .= "/".$p;
				$breadcrumb[$p] = $bc_link;
			}
		}

		$t = get_markup_template( "_breadcrumbs.tpl", "addon/projects/" );
		return replace_macros($t, array(
			'$breadcrumb' => $breadcrumb,
			'$sel_branch' => $sel_branch,
			'$is_file' => $is_file
		));
	}


	private function _tree(&$vcs, $ref, $path) {
		function comp($a, $b) {
			return $a['sort']<$b['sort'] ? -1 : 1;
		}

		$tree = $vcs->getTree($ref, $path);

		if ($path!="") {
			$path.="/";
			$tree[] = [
				'sort' => "0:..",
				'file' => "..",
				'type' => "tree",
			];
		}
		usort ( $tree, 'comp' );

		foreach($tree as &$obj) {
			if ($obj['file']!="..") {
				$obj['log'] = $vcs->getLog($ref, $path.$obj['file'])[0];
			}
			switch($obj['type']){
				case 'tree':
				case 'blob':
					//~ $obj['link'] = "./projects/{$vcs->user}/{$vcs->slug}/{$ref}/".$path.$obj['file'];
					$obj['link'] = $this->route_reverse("tree", [
						'user'=>$vcs->user,
						'slug'=>$vcs->slug,
						'ref' =>$ref,
						'path'=>$path.$obj['file']
					]);
					break;
			}
		}



		$t = get_markup_template( "_tree.tpl", "addon/projects/" );
		$o = replace_macros($t, array(
			'$tree' => $tree
		));

		return $o;
	}


	/**
	 * Project tree browsing page
	 */
	private function tree($user, $slug, $ref, $path) {
		if (!$this->load_user($user)) return $this->notfound();

		$perm_sql = permissions_sql($this->uid);
		$list = q("SELECT id FROM projects WHERE uid=%d AND name='%s' %s", $this->uid, dbesc($slug), $perm_sql);
		if ($list===false || count($list)==0) return $this->notfound();

		$vcs = $this->vcs($user, $slug);
		if ($vcs===false) return $this->notfound();

		// I can't find a quick way to decide if $path is a folder
		// or a file. I use it as a folder. If an exception is raised,
		// then could be a file.
		// But still, I can't find a quick way to get the hash of a file
		// from his name, so I get the tree of the containig folder,
		// loop it, and check the file name.
		try {
			$tree = $this->_tree($vcs, $ref, $path);
		} catch(PHPGit\Exception\GitException $e) {

			$dir = dirname($path);
			$name = basename($path);
			if ($dir===".") $dir="";
			foreach($vcs->getTree($ref, $dir) as $entry) {
				if ($entry['type']=='blob' && $entry['file']==$name) return $this->blob($user, $slug, $entry['hash'], $ref, $path);
			}
			return $this->notfound();
		}
		$breadcrumb = $this->_breadcrumbs($vcs, $ref, $path);

		// origin remote
		$remote = $vcs->getRemote();
		if (is_null($remote)) $remote = 'new';

		$t = get_markup_template( "tree.tpl", "addon/projects/" );
		$o = replace_macros($t, array(
			'$slug' => $slug,
			'$user' => $user,
			'$ref' => $ref,
			'$breadcrumb' => $breadcrumb,
			'$tree' => $tree,
			'$remote' => $remote,
			'$isowner' => $this->is_owner()
		));

		return $o;

	}

	/**
	 * Display blob content page
	 */
	private function blob($user, $slug, $hash, $ref=null, $path=null) {
		if (!$this->load_user($user)) return $this->notfound();
		$vcs = $this->vcs($user, $slug);
		if ($vcs===false) return $this->notfound();

		if ($path==null && $ref==null) {
			// is $user/$slug/blob/$hash. dump the content to the browser
			// try to detect mime
			if (class_exists('finfo')) {
				$fi = new finfo(FILEINFO_MIME_TYPE);
				$mime =  $fi->buffer( $vcs->getBlob($hash) );
				header('Content-Type: '.$mime);
			}
			echo $vcs->getBlob($hash);
			killme();
		}



		$types = [
			'css'=>'css', 'php'=>'php',
			'js'=>'javascript', 'sh'=>'shell',
			'bash'=>'shell', 'sql'=>'sql',
			'html'=>'xml', 'xml'=>'xml',
			'twig'=>'twig', 'tpl'=>'twig'
		];

		$maintype=null;
		$type=null;
		if ($path!=="") {
			if (class_exists('finfo')) {
				$fi = new finfo(FILEINFO_MIME_TYPE);
				$mime =  $fi->buffer( $vcs->getBlob($hash) );
				list($maintype, $type) = explode("/", $mime);
				$type = str_replace("x-", "", $type);
				if (!x($types,$type)) $type=null;
			}

			if ($type==null) {
				$ext = array_pop(explode(".", basename($path)));
				if (x($types,ext)) $type=$ext;
			}
		}

		if ($maintype == 'image') {
			$type = "image";
			$code = $hash;
		} elseif ($maintype == 'application') {
			$type = "binary";
			$code = $hash;
		} elseif ($ext == 'md') {
			$type = 'markdown';
			$code = $this->_md2html($user, $slug, $ref, $vcs->getBlob($hash));
		} elseif ($type!=null) {
			$type = $types[$type];
			$hl = new Hyperlight\Hyperlight($type);
			$code = $hl->render( $vcs->getBlob($hash) );
		} else {
			$type = "text";
			$code = htmlspecialchars($vcs->getBlob($hash));
		}

		$breadcrumb = $this->_breadcrumbs($vcs, $ref, $path, true);

		// origin remote
		$remote = $vcs->getRemote();
		if (is_null($remote)) $remote = 'new';

		$t = get_markup_template( "blob.tpl", "addon/projects/" );
		$o = replace_macros($t, array(
			'$slug' => $slug,
			'$user' => $user,
			'$ref' => $ref,
			'$hash' => $hash,
			'$breadcrumb' => $breadcrumb,
			'$type' => $type,
			'$code' => $code,
			'$path' => $path,
			'$remote' => $remote,
			'$isowner' => $this->is_owner()

		));

		return $o;

	}

	/**
	 * Commits page
	 */
	private function commits($user, $slug, $ref) {
		if (!$this->load_user($user)) return $this->notfound();
		$vcs = $this->vcs($user, $slug);
		if ($vcs===false) return $this->notfound();


		if (!$vcs->isCommit($ref)) return $this->notfound();


		// branch log
		if (isset($vcs->getBranches()[$ref])) {
			$logs = array();
			$lastdate = null;
			foreach ($vcs->getLog($ref) as $l) {
				$l['details'] = $this->commit_author_details($l);

				$date = strtotime(implode(" ",array_slice(explode(" ",$l['date']),0,4)));
				if ($lastdate != $date) {
					$lastdate = $date;
					$logs[$date] = array();
				}
				$logs[$date][] = $l;
			}

			$t = get_markup_template( "commits.tpl", "addon/projects/" );
			return replace_macros($t, array(
				'$breadcrumb' => $this->_branchselector($vcs, $ref, "commits/%s"),
				'$user' => $user,
				'$slug' => $slug,
				'$ref' => $ref,
				'$logs' => $logs
			));

		}


		// commit log
		$commit = $vcs->getLog($ref, "" ,1)[0];
		$commit['details'] = $this->commit_author_details($commit);
		$commit['message'] = "";
		$difs = array();

		$buffer = explode("\n", $vcs->getCommit($ref) );
		$flag=0;
		foreach ($buffer as $line) {

			if (substr($line,0,5)=="Merge") $commit['merge']= explode(" ",trim(substr($line,6)));

			if (substr($line,0,4)=="diff") $flag=2;

			if ($flag==2) $difs[$dif] .= $line."\n";

			if ($flag==1) {
				$commit['message'].=substr($line,4)."\n";
			}

			if ($flag==0 && $line=="") $flag=1;
		}
		if (x($commit,'merge')) $difs = $vcs->diff($commit['merge'][0],$commit['merge'][1]);

		$commit['message'] = substr($commit['message'], strlen($commit['title'])+1);
		$commit['message'] = bbcode(diaspora2bb($commit['message'], false, false));

		$t = get_markup_template( "commit.tpl", "addon/projects/" );
		return replace_macros($t, array(
			'$user' => $user,
			'$slug' => $slug,
			'$ref' => $ref,
			'$commit' => $commit,
			'$difs' => $difs
		));

	}

	/**
	 * convert array to xml
	 *
	 * @param string $root	root element
	 * @param array $arr	array to convert
	 *
	 * @returns string
	 */
	private function arr2xml($root, $arr) {
		$o = "<$root>";
		foreach($arr as $k => $v) {
			if (is_array($v)) {
				$o .= $this->arr2xml($k, $v);
			} else {
				$o .= "<$k>$v</$k>";
			}
		}
		$o .= "</$root>";
		return $o;
	}

	/**
	 * page to create a new pull request
	 */

	function pr_startnew($user, $slug, $ref) {
		if (!local_user()) return $this->notfound();
		if (!$this->load_user($user)) return $this->notfound();
		if (!$this->load_project($slug)) return $this->notfound();
		if (!$this->is_owner()) return $this->notfound();

		$repo = $this->project;

		$is_public = ($repo['allow-cid'].$repo['allow-gid'].$repo['deny-cid'].$repo['deny-gid']) == "";


		if (!$is_public) {
			return "We cannot create pull requests for private repository yet.";
		}

		/* get vcs object */
		$vcs = $this->vcs($user, $slug);

		$remote = $vcs->getRemote();


		/* if an open pull request for this branch exists, redirect */
		$r = q("SELECT guid FROM projects_pr WHERE status=%d AND pid=%d AND head_ref='%s'",
				PROJECTS_PR_STATUS_SENT,
				intval($repo['id']),
				dbesc($ref)
			);
		if ($r!==false && count($r)>0) {
			goaway( $this->a->get_baseurl(). "/display/". $r[0]['guid'] );
			return;
		}


		// find upstream contact

		$m = [];
		preg_match("|.*//(.*)/projects/([^/]*)|", $remote['url'], $m);
		$nurl = "http://".$m[1]."/profile/".$m[2];
		$contact = q("SELECT * FROM contact WHERE uid=%d AND nurl='%s'", $this->uid, dbesc($nurl));
		if ($contact===false || count($contact)==0) return t("Upstream contact not found.");
		$contact = $contact[0];

		$bref = "master";
		$title = "";
		$description = "";

		if (x($_POST, "form_security_token")) {
			check_form_security_token_redirectOnErr(
				$this->route_reverse('pr_startnew',[
					'user'=>$user,
					'slug'=>$slug,
					'ref'=>$ref
				]),
				'pullrequest_new'
			);

			$bref = $_POST['base_fork'];
			$ref = $_POST['head_fork'];

			$title = x($_POST,'title') ? $_POST['title'] : "";
			$description = x($_POST,'description') ? $_POST['description'] : "";

			if ($title==="") {
				notice( t("Title is required") );
			} else {

				// send pull request item
				$uri = item_new_uri($this->a->get_hostname(), $this->uid);
				$guid = get_guid(32);
				$owner_record = $this->get_user_record();

				$remote = $vcs->getRemote();
				$objectplink = $this->a->get_baseurl()."/projects/$user/$slug/pull/".urlencode($guid);
				$object = [
					'type' => OBJECT_PULLREQUEST,
					'id' => $guid,
					'link' => $this->repocloneurl($user, $slug, $public)."#".$ref.":".$remote['name']."/".$bref,
					'title' => '', //$title,
					'content' => '', //$description,
				];
				$object = $this->arr2xml("object", $object);
				$objecttype = OBJECT_PULLREQUEST;

				//echo "<pre>"; var_dump($object); killme();

				//save local pull request object
				$rins = q("INSERT INTO projects_pr
					(guid, pid, status, head_url, head_ref, base_url, base_ref) VALUES
					('%s', %d, %d , '%s', '%s', '%s', '%s');",
					dbesc($guid),
					intval($this->project['id']),
					PROJECTS_PR_STATUS_SENT,
					dbesc($this->project['url']),
					dbesc($ref),
					dbesc($remote['repourl']),
					dbesc($bref)
				);

				//upstream contact tag
				$tag = sprintf("@[url=%s]%s[/url]",$contact['url'],$contact['name']);


				// Create item container
				$arr = array();
				$arr['guid'] = $guid;
				$arr['uid'] = $this->uid;
				$arr['uri'] = $uri;
				$arr['parent-uri'] = $uri;
				$arr['type'] = 'project.pullrequest';
				$arr['wall'] = 1;
				$arr['contact-id'] = $owner_record['id'];
				$arr['owner-name'] = $owner_record['name'];
				$arr['owner-link'] = $owner_record['url'];
				$arr['owner-avatar'] = $owner_record['thumb'];
				$arr['author-name'] = $owner_record['name'];
				$arr['author-link'] = $owner_record['url'];
				$arr['author-avatar'] = $owner_record['thumb'];
				$arr['title'] = '['.$slug.'] '.$title;
				$arr['allow_cid'] = "<".$contact['id'].">";
				$arr['allow_gid'] = "";
				$arr['deny_cid'] = "";
				$arr['deny_gid'] = "";
				$arr['last-child'] = 1;
				$arr['visible'] = 1;
				$arr['origin'] = 1;
				$arr['body'] = $description."\n\n\n".$tag;
				$arr['tag'] = $tag;
				$arr['plink'] = $objectplink;
				$arr['object-type'] = $objecttype;
				$arr['object'] = $object;
				#var_dump($arr); killme();

				$item_id = item_store($arr);
				proc_run('php', "include/notifier.php", 'wall-new', $item_id);
				goaway( $objectplink );
				return;
			}
		}


		$local_branches_select = array_keys($vcs->getBranches());
		$local_branches_select = array_combine($local_branches_select, $local_branches_select);

		$remote_branches_select = array_keys($vcs->getRemoteBranches());
		$remote_branches_select = array_combine($remote_branches_select, $remote_branches_select);

		$base_ref = $vcs->getRemoteBranches()[$bref]['hash'];
		$head_ref = $vcs->getBranches()[$ref]['hash'];


		/* create a temporary PR object to show diff */
		$tmp_vcs = $vcs->pr_get_temp_vcs([
			'guid' => 'tmp_'.$ref.'_'.$bref,
			'pid' => $this->project['id'], // ?
			'status' => PROJECTS_PR_STATUS_SENT,
			'head_url' => $this->project['url'],
			'head_ref' => $ref,
			'base_url' => $remote['repourl'],
			'base_ref' => $bref
		]);
		$ni = $this->get_identity();
		$tmp_vcs->setIdentity( $ni['username'], $ni['sitelocation'] );
		$tmp_vcs->update();

		try {
			$diff = $tmp_vcs->get_pr_diff();
		} catch(\Exception $e) {
			$diff = null;
		}

#		echo "<pre>"; echo $diff . "</pre><br><hr><br>". format_diff($diff); killme();

		$t = get_markup_template( "pullrequest_new.tpl", "addon/projects/" );
		return replace_macros($t, array(
			'$baseurl' => $this->a->get_baseurl(),
			'$user' => $user,
			'$slug' => $slug,
			'$ref' => $ref,
			'$remote' => $remote,
			'$isowner' => $this->is_owner(),
			'$contact' => $contact,
			'$title' => ['title', t('Title'), $title, t("Pull request title (required)")],
			'$description' => ['description', t('Description'), $description, "@".$contact['name']."<br>".t("Pull request description (optional, can use BBCode)")],
			'$head_fork' => ['head_fork', sprintf(t("head fork: %s"), $user."/".$slug), $ref, "", $local_branches_select],
			'$base_fork' => ['base_fork', sprintf(t("base fork: %s"), $remote['name']), $bref, "", $remote_branches_select],
			'$head_ref' => $head_ref,
			'$base_ref' => $base_ref,
			'$diff' => $diff,
			'$form_security_token' => get_form_security_token("pullrequest_new")
		));


	}

	function pr_startnew_post($user, $slug, $ref) {
		// friendica call func_post() AND func()
		// I'm handling POST into main function, nothing to do here.
	}

	/**
	 * list pull requests page
	 */
	function pr_list($user, $slug) {
		if (!$this->load_user($user)) return $this->notfound();
		if (!$this->load_project($slug)) return $this->notfound();

		$repo = $this->project;

		$vcs = $this->vcs($user, $slug);
		if ($vcs===false) return $this->notfound();

		$_route_arg = ['user'=>$user,'slug'=>$slug];

		$filter = ((x($_GET,'filter'))?$_GET['filter']:'');

		$sql_extra = "";
		switch ($filter){
			case '': $sql_extra = sprintf("AND pr.status=%d", intval(PROJECTS_PR_STATUS_NEW)); break;
			case 'closed':  $sql_extra = sprintf("AND pr.status=%d", intval(PROJECTS_PR_STATUS_CLOSED)); break;
			case 'merged':  $sql_extra = sprintf("AND pr.status=%d", intval(PROJECTS_PR_STATUS_MERGED)); break;
			case 'sent':  $sql_extra = sprintf("AND pr.status=%d", intval(PROJECTS_PR_STATUS_SENT)); break;
			case 'all': break;
			default:
				goaway($this->route_reverse("pr_list",$_route_arg));
		}

		$r = q("SELECT * FROM projects_pr as pr
			LEFT JOIN item ON pr.guid=item.guid
			WHERE pr.pid=%d AND item.uid=%d %s",
				intval($repo['id']),
				intval($this->uid),
				$sql_extra
		);

		//count PRs
		$count = [0,0,0,0];
		$_count = q("SELECT count(pr.guid) as tot, pr.status
						 FROM projects_pr as pr
					  	 LEFT JOIN item ON pr.guid=item.guid
						 WHERE pr.pid=%d AND item.uid=%d GROUP BY pr.status",
					intval($repo['id']),
					intval($this->uid)
				);
		if ($_count!==false) {
			foreach ($_count as $c)
				$count[intval($c['status'])] = $c['tot'];
		}
		$count_all = array_sum($count);


		$filters = [
			[
				'label' => t('Open')." ({$count[PROJECTS_PR_STATUS_NEW]})",
				'url'	=> $this->route_reverse("pr_list",$_route_arg),
				'sel' 	=> ($filter==""?'active':''),
				'id' => 'projects-pr-open-filter',
			],
			[
				'label' => t('Closed')." ({$count[PROJECTS_PR_STATUS_CLOSED]})",
				'url'	=> $this->route_reverse("pr_list",$_route_arg)."filter=closed",
				'sel' 	=> ($filter=="closed"?'active':''),
				'id' => 'projects-pr-close-filter',
			],
			[
				'label' => t('Merged')." ({$count[PROJECTS_PR_STATUS_MERGED]})",
				'url'	=> $this->route_reverse("pr_list",$_route_arg)."filter=merged",
				'sel' 	=> ($filter=="merged"?'active':''),
				'id' => 'projects-pr-merge-filter',
			],
			[
				'label' => t('Sent')." ({$count[PROJECTS_PR_STATUS_SENT]})",
				'url'	=> $this->route_reverse("pr_list",$_route_arg)."filter=sent",
				'sel' 	=> ($filter=="sent"?'active':''),
				'id' => 'projects-pr-sent-filter',
			],
			[
				'label' => t('All')." ({$count_all})",
				'url'	=> $this->route_reverse("pr_list",$_route_arg)."filter=all",
				'sel' 	=> ($filter=="all"?'active':''),
				'id' => 'projects-pr-all-filter',
			],
		];

		// origin remote
		$remote = $vcs->getRemote();
		if (is_null($remote)) $remote = 'new';

		$t = get_markup_template( "pullrequest_list.tpl", "addon/projects/" );
		return replace_macros($t, [
			'$baseurl' => $this->a->get_baseurl(),
			'$user' => $user,
			'$slug' => $slug,
			'$remote' => $remote,
			'$isowner' => $this->is_owner(),
			'$filters' => $filters,
			'$list' => $r
		]);
	}

	function pr_close ($item, $tmp_vcs) {
		$return_url = $this->route_reverse('pr_show',['user'=>$this->user,'slug'=>$this->project['name'],'id'=>$item['guid'],'tab'=>'']);
		$return_url_post = str_replace($this->a->get_baseurl(),"", $return_url);


		if ($item['status']==PROJECTS_PR_STATUS_CLOSED) {
			goaway($return_url);
			return;
		}


		q("update projects_pr set status=%d, closed='%s' WHERE guid='%s' AND pid=%d",
			PROJECTS_PR_STATUS_CLOSED,
			dbesc(datetime_convert()),
			dbesc($item['guid']),
			intval($item['pid'])
		);


		// send message
		$_REQUEST = [
			'api_source' => true,
			'return' => $return_url_post,
			'profile_uid' => $this->uid,
			'body' => 'Pull request closed',
			'parent' => $item['id'],
			'verb' => PROJECTS_VERB_PR_CLOSE,
			'object-type' => OBJECT_PULLREQUEST,
			'object' => $item['object'], #this isn't passed to remotes. why? idk.
										 # we don't need it anyway. we use guid from item
		];

		item_post($this->a);
		goaway($return_url);
		killme();
	}

	function pr_merge($item, $tmp_vcs) {
		$return_url = $this->route_reverse('pr_show',['user'=>$this->user,'slug'=>$this->project['name'],'id'=>$item['guid'],'tab'=>'']);
		$return_url_post = str_replace($this->a->get_baseurl(),"", $return_url);


		if ($item['status']==PROJECTS_PR_STATUS_MERGED) {
			goaway($return_url);
			return;
		}

		// merge pr branch
		$r = $tmp_vcs->pr_merge();

		if ($r===false) {
			notice( t('Pull request cannot merged automatically.') . EOL) ;
			goaway($return_url);
			return;
		}

		q("update projects_pr set status=%d, closed='%s', merge_ref='%s' WHERE guid='%s' AND pid=%d",
			PROJECTS_PR_STATUS_MERGED,
			dbesc(datetime_convert()),
			dbesc($r),
			dbesc($item['guid']),
			intval($item['pid'])
		);


		// send message
		$_REQUEST = [
			'api_source' => true,
			'return' => $return_url_post,
			'profile_uid' => $this->uid,
			'body' => 'Pull request merged',
			'parent' => $item['id'],
			'verb' => PROJECTS_VERB_PR_MERGE,
			'object-type' => OBJECT_PULLREQUEST,
			'object' => $item['object'], #this isn't passed to remotes. why? idk.
										 # we don't need it anyway. we use guid from item
		];

		item_post($this->a);
		goaway($return_url);
		killme();
	}

	function get_identity($uid=null) {
		if (is_null($uid)) $uid=$this->uid;

		$r = q("SELECT nickname, username FROM user WHERE uid=%d",
				intval($uid)
			);
		$u = $r[0];
		$u['sitelocation'] = $u['nickname'] . "@" . substr($this->a->get_baseurl(),strpos($this->a->get_baseurl(),'//') + 2 );
		return $u;
	}

	/**
	 * show pull requests page
	 */
	function pr_show($user, $slug, $id, $tab="") {
		if (!$this->load_user($user)) return $this->notfound();
		if (!$this->load_project($slug)) return $this->notfound();


		$repo = $this->project;

		$vcs = $this->vcs($user, $slug);
		if ($vcs===false) return $this->notfound();
		$remote = $vcs->getRemote();

		$r = q("SELECT * FROM projects_pr as pr
			LEFT JOIN item ON pr.guid=item.guid
			WHERE pr.guid='%s' AND pr.pid=%d AND item.uid=%d",
				dbesc($id),
				intval($this->project['id']),
				intval($this->uid)
		);
		if ($r===false || count($r)===0) return $this->notfound();

		$item = $r[0];

		$tmp_vcs = $vcs->pr_get_temp_vcs($item);
		$ni = $this->get_identity();
		$tmp_vcs->setIdentity( $ni['username'], $ni['sitelocation'] );
		$tmp_vcs->update();

		$tab = str_replace("/","", $tab);

		if ($tab==="close") {
			$this->pr_close($item, $tmp_vcs);
			return;
		}
		if ($tab==="merge") {
			$this->pr_merge($item, $tmp_vcs);
			return;
		}


		$diff = $tmp_vcs->get_pr_diff();
		//TODO: get_pr_logs() should rebase also?
		$logs = $tmp_vcs->get_pr_logs();

		$count_logs = count($logs);
		$count_diff = count( explode("diff --git", $diff) ) - 1;


		$tabs = [
			[
				'label' => t('Conversation'),
				'url'	=> $this->route_reverse("pr_show",['user'=>$user,'slug'=>$slug,'id'=>$id, 'tab'=>'']),
				'sel' 	=> ($tab==""?'active':''),
				'id' => 'projects-pr-conversation-tab',
			],
			[
				'label' => t('Commits')." ($count_logs)",
				'url'	=> $this->route_reverse("pr_show",['user'=>$user,'slug'=>$slug,'id'=>$id, 'tab'=>'/commits']),
				'sel' 	=> ($tab=="commits"?'active':''),
				'id' => 'projects-pr-commits-tab',
			],
			[
				'label' => t('Files changed')." ($count_diff)",
				'url'	=> $this->route_reverse("pr_show",['user'=>$user,'slug'=>$slug,'id'=>$id, 'tab'=>'/diff']),
				'sel' 	=> ($tab=="diff"?'active':''),
				'id' => 'projects-pr-diff-tab',
			],
		];




		switch($tab) {
			case "": {
				$thread_html = $this->get_comments($item['id']);
				$t = get_markup_template( "pullrequest_show.tpl", "addon/projects/" );
				return replace_macros($t, [
					'$baseurl' => $this->a->get_baseurl(),
					'$url' => $this->route_reverse('pr_show', ['user'=>$user, 'slug'=>$slug, 'id'=>$id, 'tab'=>$tab]),
					'$user' => $user,
					'$slug' => $slug,
					'$remote' => $remote,
					'$isowner' => $this->is_owner(),
					'$tabs' => $tabs,
					'$item' => $item,
					'$thread_html' => $thread_html,
				]);
			}; break;
			case "commits": {
				$logs = [];
				$lastdate = null;
				foreach ($tmp_vcs->get_pr_logs() as $l) {
					$l['details'] = $this->commit_author_details($l);

					$date = strtotime(implode(" ",array_slice(explode(" ",$l['date']),0,4)));
					if ($lastdate != $date) {
						$lastdate = $date;
						$logs[$date] = [];
					}
					$logs[$date][] = $l;
				}

				$t = get_markup_template( "pullrequest_show_commits.tpl", "addon/projects/" );
				return replace_macros($t, [
					'$baseurl' => $this->a->get_baseurl(),
					'$remote' => $remote,
					'$user' => $user,
					'$slug' => $slug,
					'$ref' => $ref,
					'$tabs' => $tabs,
					'$item' => $item,
					'$logs' => $logs
				]);

			}; break;
			case "diff": {
				$t = get_markup_template( "pullrequest_show_diff.tpl", "addon/projects/" );
				return replace_macros($t, [
					'$baseurl' => $this->a->get_baseurl(),
					'$remote' => $remote,
					'$user' => $user,
					'$slug' => $slug,
					'$ref' => $ref,
					'$tabs' => $tabs,
					'$item' => $item,
					'$diff' => $tmp_vcs->get_pr_diff()
				]);
			}
			default:
				return $this->notfound();
		}
	}

	/**
	 * return formatted diff between $ref1 and $ref2
	 */
	function ajax_diff($user, $slug, $ref1, $ref2) {
		if (!$this->load_user($user)) $this->ajax_error();
		if (!$this->is_owner())  $this->ajax_error();

		$vcs = $this->vcs($user, $slug);
		if ($vcs===false)  $this->ajax_error();

		try {
			echo format_diff($vcs->diff($ref1, $ref2));
		} catch(\Exception $e) {
			 $this->ajax_error(t("Can't show you the diff at the moment. Sorry"));
		}

		killme();
	}

	function ajax_error($msg="error"){
		echo "<div class='pjs-empty'>$msg</div>"; killme();
	}

	/**
	 * authenticate user on project
	 *
	 * if project's acl is "everyone", everyone can get it
	 * else:
	 * - check remote user
	 * 		- is set?
	 * 			 - is allowed?
	 * 				- get it
	 * 		- is PHP_AUTH_USER set and it contains '@'?
	 * 			- remote auth the PHP_AUTH_USER to the repo oner
	 * - login owner via login_api()
	 *
	 * @param bool $only_owner
	 *      false: check for public/private, try to auth remote contacts
	 *      true: owner must be authenticated, regardless of ACL
	 */
	public function auth( $slug, $only_owner) {
		$a = get_app();
		$uid = $this->uid;

		$r = q("SELECT * FROM projects WHERE name='%s' AND uid=%d",
			dbesc($slug),
			$uid
		);
		if ($r===false || count($r)===0) return false;

		$project = $r[0];

		if ($only_owner) {
			if (!api_user()) {
				api_login($a);
			}
			if (api_user()!= $uid) return false;

		} else {
			$public = ($project['allow_cid'].$project['allow_gid'].$project['deny_cid'].$project['deny_gid'] == "" );

			if (!$public) {
				// private repo.
				$u = explode(":", base64_decode(substr($_SERVER["HTTP_AUTHORIZATION"],6)))[0];
				if (strstr($u,"@")) {
					// contact tries to login

					list($nick, $server) = explode("@", $u);
					// search for contact
					$c = q("SELECT id, url FROM contact WHERE
							network='%s' AND uid=%d AND nick='%s' AND url LIKE '%%%s%%'",
							dbesc(NETWORK_DFRN),
							$uid,
							dbesc($nick),
							dbesc($server));
					if ($c===false || count($c)===0) return false;
					$c = $c[0];
					// is already logged in?
					if (remote_user() !== $r['id']) {
						// no, redirect
						$owner_profile = $a->get_baseurl()."/profile/$user";
						$this_url = $a->get_baseurl()."/projects/$user/$slug/$path";

						$url = array_slice(explode("/", $c['url']),0,3);
						$url[2] = $nick."@".$url[2];
						$url = implode("/", $url);
						$url = $url.sprintf("/api/friendica/remoteauth?c_url=%s&url=%s",
							urlencode( $owner_profile),
							urlencode( $this_url ));

						goaway($url);
					}
					// yes.. go on
				} else {

					// owner tries to log in
					if (!api_user()) {
						api_login($a);
					}
					if (api_user()!= $uid) return false;
				}
			}

			// check permissions
			$perm_sql = permissions_sql($uid);
			$list = q("SELECT * FROM projects WHERE uid=%d AND name='%s' %s", $uid, dbesc($slug), $perm_sql);
			if ($list===false || count($list)==0) return false;
		} // /only_owner


		return true;
	}



	/**
	 * http VCS proxy
	 */
	private function serve_post($user, $slug, $path) {
		ini_set('html_errors', 0);

		if (!$this->load_user($user)) return $this->notfound(true);

		$vcs = $this->vcs($user, $slug);

		try {
			$vcs->serve_post($path);
		} catch (VCS\NotAuthorizedException $e) {
			return $this->notfound(true);
		}

		killme();
	}

	private function serve($user, $slug, $path) {

		ini_set('html_errors', 0);

		if (!$this->load_user($user)) return $this->notfound(true);

		$vcs = $this->vcs($user, $slug);

		try {
			$vcs->serve($path);
		} catch (VCS\NotAuthorizedException $e) {
			return $this->notfound(true);
		}

		killme();
	}
}





/**
 * template var filter
 *
 * use:
 * {{$blobstring|numerize}}
 */
function numerize($str){
	$lines = explode("\n", $str);
	foreach($lines as $k=>&$v) {
		$n=$k+1;
		$v = "<div id='L$n' class='linehi'><a name='L$n' href='#L$n' class='linenumber'>$n</a>$v</div>";
	}
	return implode("\n", $lines);
}
/**
 * template var filter
 *
 * use:
 * {{$diffstr|format_diff}}
 */
function format_diff($str) {
	$lines = explode("\n", $str);
	$o = "";
	$line_add=0;
	$line_del=0;

	$flag=0;

	foreach($lines as $line){
		$side_add = "&nbsp;";
		$side_del = "&nbsp;";

		if (substr($line,0,7)=="deleted") {
			$flag=0;
		}


		if (substr($line,0,5)=="index") {
			continue;
		}

		if (substr($line,0,5)=="+++ b") {
			$flag=0;
			continue;
		}


		if ($flag===1) continue;
		if (substr($line,0,10)=="diff --git") {
			if ($o!="") $o .= "</table></div>";

			$filename = str_replace("a/", "", trim(explode(" ", $line)[2]));

			$o .= "<div class='pjs-diff-file'>".$filename."</div>";
			$o .= "<div class='pjs-diff-fmt'><table>";
			$flag = 1;
			continue;
		}

		if (substr($line,0,3)=="@@ ") {
			$class = 'pjs-diff-head';
			$match = array();
			preg_match("|-(\d+),.*\+(\d+),|",explode("@@", $line)[1], $match);
			$line_del = intval($match[1]);
			$line_add = intval($match[2]);
		}
		elseif (substr($line,0,1)=="+") {
			$class = 'pjs-diff-add';
			$side_add = $line_add;
			$line_add++;
		}
		elseif (substr($line,0,1)=="-") {
			$class = 'pjs-diff-sub';
			$side_del=$line_del;
			$line_del++;
		}
		else {
			$class = 'pjs-diff-comm';
			$side_add = $line_add;
			$side_del = $line_del;
			$line_add++;
			$line_del++;
		}

		$o .= sprintf("<tr class='%s'><td class='pjs-diff-side'>%s</td><td class='pjs-diff-side'>%s</td><td>%s</td></tr>",
				$class,
				$side_del,
				$side_add,
				htmlspecialchars($line)
			);
	}
	$o .= "</table></div>";
	return $o;
}

/* addon interface */

function projects_install() {
	$schema = [
		"projects" => [
			"fields" => [
				"id" => ["type" => "int(11)", "not null" => "1", "extra" => "auto_increment", "primary" => "1"],
				"uid" => ["type" => "int(11)", "not null" => "1"],
				"name" => ["type" => "char(255)", "not null" => "1"],
				"type" => ["type" => "char(5)", "not null" => "1", "default" => "git"],
				"allow_cid" => ["type" => "mediumtext", "not null" => "1"],
				"allow_gid" => ["type" => "mediumtext", "not null" => "1"],
				"deny_cid" => ["type" => "mediumtext", "not null" => "1"],
				"deny_gid" => ["type" => "mediumtext", "not null" => "1"]
			],
			"indexes" => [
				"PRIMARY" => ["id"],
				"uid_name" => ["uid","name"]
			],
		],
		"projects_pr" => [
			"fields" => [
				"guid" => ["type" => "char(255)", "not null" => "1", "primary" => "1"],
				"pid" =>  ["type" => "int(11)", "not null" => "1", "primary" => "1"],
				"status" =>  ["type" => "int(11)", "not null" => "1", "default" => PROJECTS_PR_STATUS_NEW],
				"head_url" => ["type" => "char(255)", "not null" => "1"],
				"head_ref" => ["type" => "char(255)", "not null" => "1"],
				"base_url" => ["type" => "char(255)", "not null" => "1", "default"=>""],
				"base_ref"  => ["type" => "char(255)", "not null" => "1"],
				"merge_ref" => ["type" => "char(255)"],
				"closed" => ["type" => "datetime", "not null" => "1", "default" => "0000-00-00 00:00:00"],
			],
			"indexes" => [
				"PRIMARY" => ["guid","pid"],
			]
		]
	];
	$tables = q("SHOW TABLES LIKE 'projects%%'");
	$r = update_structure(false, true, $tables, $schema);

	if ($r===false) {
		register_hook('profile_tabs', 'addon/projects/projects.php', 'projects_profile_tabs');
		register_hook('nav_info', 'addon/projects/projects.php', 'projects_nav_info');
		register_hook('post_remote_end', 'addon/projects/projects.php', 'projects_post_remote_end');
		return true;
	} else {
		notice($r);
		return false;
	}
}

function projects_uninstall() {
	unregister_hook('profile_tabs', 'addon/projects/projects.php', 'projects_profile_tabs');
	unregister_hook('nav_info', 'addon/projects/projects.php', 'projects_nav_info');
	unregister_hook('post_remote_end', 'addon/projects/projects.php', 'projects_post_remote_end');
	return true;
}

function projects_nav_info (&$a, &$b) {
    if (local_user()) {
	$b['usermenu'][] = Array(
	    $a->get_baseurl() . '/projects/' . $a->user['nickname'],
	    t('Personal Repositories'),
	    "",
	    t('Your personal project repositories'));
    }
}

function projects_profile_tabs(&$a, &$b) {
	$b['tabs'][] = array(
		'label' => t('Projects'),
		'url'	=> $a->get_baseurl() . '/projects/' . $b['nickname'],
		'sel' 	=> ((!$b['tab']&&$a->argv[0]=='projects')?'active':''),
		'title' => t('Projects repository'),
		'id' => 'projects-tab',
	);
}

function projects_post_remote_end(&$a, &$b) {
	Projects::instance($a)->on_post_remote_end($a, $b);
}

function projects_module() {}

function projects_init(&$a) {
	Projects::instance($a)->init();
}

function projects_content(&$a) {
	return Projects::instance($a)->content();
}

function projects_post(&$a) {
	return Projects::instance($a)->post();
}


