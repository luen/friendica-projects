Pull requests
=============

A user which cloned a repository can send a pull request to ask upstream
owner to merge his changes.

A button to start a new pull request will be shown to the user only if the
repository is a clone.

Only the upstream owner can merge or close a pull requests.

### Pull request data structure

Pull requests are items with type `"project.pullrequest"`,
object-type `OBJECT_PULLREQUEST` (`http://kirgroup.com/schema/1.0/pullrequest`),
object as defined below and visibility restricted to upstream contact.


The object is:

    <object>
        <id>123abc...</id>
        <link>...</link>
        <title>...</title>
        <content>...</content>
    </object>

`id` will be item guid, `title` and `content` are title and body of the message (not used),
link is

    <git url of downstream repo>#<branch to merge>:<remote repo name>/<base branch>

`remote repo name` is `"<username>/<slug>"`

Upstream will republish pull request as a wall-to-wall message
with repository ACL. This will distribute pull request thread to
all upstream contacts that can see the repo.
This should work just like wall-to-wall posts to forums.

Upstream will use link object to fetch the remote branch into
`pr/<id>` branch

As we use bare repo git to store and serve repositories, to handle pull
requests, a temporary clone of the repository is created under friendica's
temp folder. This repo is cloned with "--shared" option, to try to minimize
used space.

NOTE: At the moment, this repo is never deleted, to speed up pull requests pages render.
It could be deleted: it will be recreated on the next pr page view.

A row in `projects_pr` table is saved on local system when the pull request
is created, to track status of sent pull requests.
`projects_pr` row in local system will have status `PROJECTS_PR_STATUS_SENT`.

When the item is received on remote system, a row is added to `projects_pr`,
with status `PROJECTS_PR_STATUS_NEW`.

See `projects.php::projects_install()` for table schema.


#### Example: new pull request

user `alice@example.com` send pull request to `bob@home.com`
to merge his branch `coolfeature` into `develop` of project `test`

    $item = [
        'guid' => "012345",
        'title' => "My cool feature",
        'body' => "As discussed",
        'object-type' => "project.pullrequest",
        'object' => "
            <object>
                <id>012345</id>
                <link><link href="http://example.com/projects/alice/test.git#coolfeature:bob/test/develop" /></link>
                <title>My cool feature</title>
                <content>As discussed</content>
            </object>
            ",
        ...
    }



### Updates to pull request. [TODO]

When user add commits to a branch for wich a pull request has been sended,
a comment is posted to the thread, with an human readable message
(like "alice added n commits") and verb `"update"`

When upstream receive the comment, it will update the local "pr/<id>" branch.

(NOTE: can we show commits messages in thread display?)


### Display pull request

The url

    "projects/<user>/<slug>/pull/<id>"

will show the thread (just like "display/<id>"), but with specific UI:
"Merge" and "Close" buttons, tabs to diff and commits


### Close pull request
The row in `projects_pr` table is update with `status` as `PROJECTS_PR_STATUS_CLOSED`

A comment is posted with human readable message ("Pull request closed") and verb `PROJECTS_VERB_PR_CLOSE`

When the comment is received by the request sender, his row is updated accordly


### Merge pull request

"pr/<id>" is merged into destination branch.

If merge fail, a message is show to the user and nothing else is done.

Else, The row in `projects_pr` table is update with `status` as `PROJECTS_PR_STATUS_MERGED`

A comment is posted with human readable message ("Pull request merged") and verb `PROJECTS_VERB_PR_MERGE`

When the comment is received by the request sender, his row is updated accordly


### Merge pull request by hand [TODO]



