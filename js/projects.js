$(function(){

    $("#id_sel_branch").on('change', function(e){
        var val = $(this).val();
        window.location.href = val;
    });

    $(".source-code .linenumber").each(function() {
        $(this).attr('href', location.pathname + $(this).attr('href'));
    });

});
