<?php

namespace Kirgroup\FProjects\VCS;

require_once 'include/api.php';
require_once 'include/security.php';

require_once __DIR__.'/../Git/git_proxy.php';

use \Kirgroup\FProjects\Utils;


class GIT {
    var $git;
    var $pjs;
    var $user;
    var $slug;

    static function getServeRoute(){
        return '/(?<user>[^/]+)/(?<slug>[a-zA-Z0-9_-]+)\.git(?<path>/*.*)';
    }

    static function getSlugFromRemoteUrl($remote_url) {
        return str_replace(".git", "", array_pop(explode("/", $remote_url)));
    }

    public function __construct($pjs, $user, $slug) {
        $this->pjs = $pjs;
        $this->user = $user;
        $this->slug = $slug;

        $p_path = $this->getRepoPath();

        $git = new \PHPGit\Git();
        if (is_dir($p_path)) {
            $git->setRepository($p_path);
        }
        $this->git = $git;
    }

    public function getType() { return "git"; }

    public function init(){
        $repo_path = $this->getRepoPath();
        $this->git->setRepository($repo_path);
        $this->git->init($repo_path, ['bare' => true]);
    }

    public function clone_remote($remote_url) {
        $repo_path = $this->getRepoPath();
        $this->git->clone($remote_url, $repo_path, ['bare' => true]);
        $this->git->setRepository($repo_path);
    }

    /**
     * delete repository from disk
     */
    public function delete() {
        return Utils::delTree($this->getRepoPath());
    }

    /**
     * return clone path relative to $baseurl/projects/ for this repo
     *
     * @return string
     */
    public function getClonePath() {
        return $this->user."/".$this->slug.".git";
    }

    public function getRepoPath() {
        return REPO_BASE_PATH."/".$this->user."/".$this->slug.".git";
    }

    public function getDescription() {
        $repopath = $this->getRepoPath();
        $description = "";
        if (is_file($repopath."/description")) {
            $description = file_get_contents($repopath."/description");
        }
        return $description;
    }

    public function setDescription($text) {
        $repo_path = $this->getRepoPath();
        file_put_contents($repo_path."/description", $text);
    }

    public function getRemote() {
        $remotes = $this->git->remote();
        if (x($remotes, "origin")) {
            return [
                'repourl' => $remotes['origin']['fetch'],
                'url' => str_replace(".git","",$remotes['origin']['fetch']),
                'name' => preg_replace("|.*/([^/]+/[^.]+)\.git|", '$1', $remotes['origin']['fetch'])
            ];
        }
        return null;
    }

    public function getRemoteBranches() {
        $cmd = new \Kirgroup\FProjects\Git\LsRemoteCommand($this->git);
        return $cmd("origin", ['heads'=>true]);
    }

    public function getBranches() {
        //TODO: return a vcs-generic array
        return $this->git->branch();
    }

    public function getTree($ref, $path) {
        //TODO: return a vcs-generic array
        return $this->git->tree($ref, $path);
    }

    public function getLog($ref, $path="", $limit=null) {
        $opts = [];
        if (!is_null($limit)) $opts['limit'] = $limit;

        //TODO: return a vcs-generic array
        return $this->git->log($ref, $path, $opts);
    }

    public function getRevCount($ref) {
        $cmd = new \Kirgroup\FProjects\Git\RevCountCommand($this->git);
        return $cmd($ref);
    }

    public function getBlob($ref) {
        return $this->git->cat->blob($ref);
    }


    public function getCommit($ref) {
        //TODO: return a vcs-generic array
        return $this->git->show($ref);
    }


    /**
     * return diff between $ref1 and $ref2
     */
    public function diff($ref1, $ref2) {
        $cmd = new \Kirgroup\FProjects\Git\DiffCommand($this->git);
        return $cmd($ref1, $ref2);
    }

    /**
     * check if $ref is a valid commit in repo
     *
     * @param string $ref commit hash or name
     * @return bool
     */
    public function isCommit($ref) {
        try {
            $ref_type =  $this->git->cat->type($ref);
        } catch(\PHPGit\Exception\GitException $e) {
            return false;
        }
        return $ref_type=="commit";
    }


    public function getReadme($ref) {
        try {
            $tree = $this->getTree($ref ,"");
        } catch (\Exception $e) {
            return null;
        }
        foreach ($tree as $obj){
            $fname = strtolower($obj['file']);
            if ($fname=="readme" || $fname=="readme.md") {
                return [
                    'blob' => $this->git->cat->blob($obj['hash']),
                    'name' => $fname
                ];
            }
        }
        return null;
    }


    // pull request management


    /**
     * create a temp clone of $this
     * with a new branch for $pr
     * pull from $pr
     *
     * @param Array $pr   pullrequest item
     *
     * @returns GITTemp to temp repo
     */
    function pr_get_temp_vcs( $pr){
        $trepo_path = join(DIRECTORY_SEPARATOR, [get_temppath(), "projects", $pr['pid'] ]);
        return  new GITTemp($trepo_path, $this->getRepoPath(), $pr);
    }





    // HTTP interface

    public function serve_post($path) {
        if ($path=="/git-upload-pack") {
            if (!$this->pjs->auth($this->slug, false)) throw new NotAuthorizedException();
        } else {
            if (!$this->pjs->auth($this->slug, true)) throw new NotAuthorizedException();
        }

        $repopath = $this->getRepoPath()."/";
        if ($path==="") $path="/";
        if ($path[0]!=="/") $path="/".$path;

        project_serve_repo($repopath, $path);
    }

    public function serve($path) {
        if (!$this->pjs->auth($this->slug, false)) throw new NotAuthorizedException();

        $repopath = $this->getRepoPath()."/";
        if ($path==="") $path="/";
        if ($path[0]!=="/") $path="/".$path;

        project_serve_repo($repopath, $path);
    }

}

class GITTemp extends GIT {
    var $path;
    var $pr;

    public function __construct($path, $base, $pr) {
        $this->path = $path;

        $this->git = new \PHPGit\Git();

        if (!is_dir($path)) {
            @mkdir($path, 0777, true);
            $this->git->clone($base, $path, ['shared'=>true]);
        }

        $this->git->setRepository($path);


        $remotes = $this->git->remote();

        if (!x($remotes,'base')) $this->git->remote->add('base', $pr['base_url']);
        if (!x($remotes,'head')) $this->git->remote->add('head', $pr['head_url']);

        $pr_ref = "pr/".$pr['guid'];

        // pr_from: user/slug/ref
        $_t = explode("/",$pr['head_url']);
        $_tl = count($_t);
        $pr_from = $_t[$_tl-2]."/".str_replace(".git","",$_t[$_tl-1])."/".$pr['head_ref'];

        $this->pr = [
            'head_url' => $pr['head_url'],
            'head_ref' => $pr['head_ref'],
            'base_url' => $pr['base_url'],
            'base_ref' => $pr['base_ref'],
            'merge_ref' => $pr['merge_ref'],
            'branch' => $pr_ref,
            'id' => $pr['guid'],
            'from' => $pr_from
        ];
    }

    /**
     * update base_ref from base_url and head_ref from head_url
     * then update pr branch
     */
    public function update() {
        /*
         * git fetch base base_ref
         * git fetch head head_ref
         *
         * git checkout base_ref
         * git rebase base/base_ref
         *
         * #se non esiste 'pr/<guid>' nel repo
         * git checkout -b pr/<guid> head/head_ref
         * #altrimenti
         * git checkout pr/<guid>
         * git rebase head/head_ref
         */


        $this->git->fetch('base');
        $this->git->fetch('head');

        if (!array_key_exists($this->pr['base_ref'], $this->getBranches())) {
            $this->git->branch->create($this->pr['base_ref'], 'base/'.$this->pr['base_ref']);
        }
        $this->git->checkout($this->pr['base_ref']);
        $this->git->rebase('base/'.$this->pr['base_ref']);

        if (!array_key_exists($this->pr['branch'], $this->getBranches())) {
            $this->git->branch->create($this->pr['branch'], 'head/'.$this->pr['head_ref']);
        }
        $this->git->checkout($this->pr['branch']);
        $this->git->rebase('head/'.$this->pr['head_ref']);



        /*
            git checkout develop
            git pull origin develop
            git checkout -b pull/012345 develop
            git fetch http://example.com/projects/alice/test.git coolfeature
            git rebase FETCH_HEAD
        /
        $this->git->checkout($this->pr['base_ref']);
        $this->git->pull("origin", $this->pr['base_ref']);

        if (!array_key_exists($this->pr['branch'], $this->getBranches())) {
            $this->git->branch->create($this->pr['branch'], $this->pr['base_ref']);
        }
        $this->git->checkout($this->pr['branch']);
        $this->git->fetch($this->pr['head_url'],$this->pr['head_ref']);
        $this->git->rebase("FETCH_HEAD");
        */
    }

    public function setIdentity($user_name, $user_email){
        // setup user for commit messages
        $this->git->config->set("user.name", $user_name, ['global'=>false,'system'=>false]);
        $this->git->config->set("user.email", $user_email, ['global'=>false,'system'=>false]);
    }

    public function getRepoPath() {
        return $this->path;
    }

    /**
     * returns pull request log between $this->pr['base_ref'] and $this->pr['branch']
     *
     * @returns Array logs
     */
    public function get_pr_logs() {
        $range = 'base/'.$this->pr['base_ref']."..".$this->pr['branch'];
        //~ if (!is_null($this->pr['merge_ref'])) {
            //~ $range = $this->pr['merge_ref'];
        //~ }
        //~ var_dump("git log ".$range); killme();
        return $this->git->log($range);
    }

    /**
     * returns pull request diff between $this->pr['base_ref'] and $this->pr['branch']
     *
     * @returns string diff
     */
    public function get_pr_diff() {
        $ref = $this->pr['branch'];
        $this->git->checkout($ref);
        try {
            $this->git->rebase('base/'.$this->pr['base_ref']);
        } catch (PHPGit\Exception\GitException $e) {
            $this->git->rebase->abort();
            return "rebase failed";
        }
        return $this->diff('base/'.$this->pr['base_ref'], $ref);
    }

    /**
     * merge $this->pr['branch'] into $this->pr['base_ref']
     *
     * @returns false on error, merge commit hash on success
     */
    public function pr_merge() {
        $r = true;
        $message = "Merge pull request #".$this->pr['id']." from ".$this->pr['from'];
        $this->git->checkout($this->pr['base_ref']);
        try {
            $this->git->merge($this->pr['branch'], $message, ['no-ff'=>true]);
        } catch (PHPGit\Exception\GitException $e) {
            $this->git->merge->abort();
            $r = false;
        }

        if ($r===true) {
            $this->git->push('origin', $this->pr['base_ref']);
            $r = $this->git->log('HEAD', null, ['limit'=>1])[0]['hash'];
            $this->pr['merge_ref'] = $r;
        }

        $this->git->checkout($this->pr['branch']);


        return $r;
    }

}
