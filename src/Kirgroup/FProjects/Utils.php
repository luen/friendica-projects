<?php

namespace Kirgroup\FProjects;


class Utils {

    public static function delTree($dir) {
        $files = array_diff(scandir($dir), array('.','..'));
        foreach ($files as $file) {
            (is_dir("$dir/$file")) ? Utils::delTree("$dir/$file") : unlink("$dir/$file");
        }
        return rmdir($dir);
    }

}
