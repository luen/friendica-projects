<?php

namespace Kirgroup\FProjects\Git;

use PHPGit\Command;

/**
 * Count revisions
 *
 * @author Fabio Comuni <fabrix.xm@gmail.com>
 * @author Kazuyuki Hayashi <hayashi@valnur.net>
 */
class RevCountCommand extends Command
{

    /**
     * Count revisions
     *
     * ``` php
     * $git = new PHPGit\Git();
     * $git->setRepository('/path/to/repo');
     * $revcount = $git->revcount();
     * ```
     *
     * ##### Output Example
     *
     * ``` php
     * 64
     * ```
     * @param string|array|\Traversable $commits [optional] Defaults to HEAD
     *
     * @return integer
     */
    public function __invoke($commits = 'HEAD')
    {
        $builder = $this->git->getProcessBuilder()
            ->add('rev-list')
            ->add('--count');

        if (!is_array($commits) && !($commits instanceof \Traversable)) {
            $commits = array($commits);
        }

        foreach ($commits as $commit) {
            $builder->add($commit);
        }


        $process = $builder->getProcess();

        $output = $this->git->run($process);
        $result = intval($output);
        return $result;
    }

}
