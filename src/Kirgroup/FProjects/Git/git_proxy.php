<?php
/* code from https://github.com/crc8/GitSmartHTTPwithPHP */



if (!function_exists("getallheaders")){
  function getallheaders() {
    foreach ($_SERVER as $name => $value) {
      if (substr($name, 0, 5) == 'HTTP_') {
        $name = str_replace(' ', '-', ucwords(strtolower(str_replace('_', ' ', substr($name, 5)))));
        $headers[$name] = $value;
      } else if ($name == "CONTENT_TYPE") {
        $headers["Content-Type"] = $value;
      } else if ($name == "CONTENT_LENGTH") {
        $headers["Content-Length"] = $value;
      }
    }
  return $headers;
  }
}

function project_serve_repo($git_project_path, $path_info) {

  $request_headers = getallheaders();
  $php_input = file_get_contents("php://input");
  $env = array
  (
    "GIT_PROJECT_ROOT"    => $git_project_path,
    "GIT_HTTP_EXPORT_ALL" => "1",
    "REMOTE_USER"         => isset($_SERVER["REMOTE_USER"])          ? $_SERVER["REMOTE_USER"]          : REMOTE_USER,
    "REMOTE_ADDR"         => isset($_SERVER["REMOTE_ADDR"])          ? $_SERVER["REMOTE_ADDR"]          : "",
    "REQUEST_METHOD"      => isset($_SERVER["REQUEST_METHOD"])       ? $_SERVER["REQUEST_METHOD"]       : "",
    "PATH_INFO"           => $path_info,
    "QUERY_STRING"        => isset($_SERVER["QUERY_STRING"])         ? $_SERVER["QUERY_STRING"]         : "",
    "CONTENT_TYPE"        => isset($request_headers["Content-Type"]) ? $request_headers["Content-Type"] : "",
  );

  $settings = array
  (
    0 => array("pipe", "r"),
    1 => array("pipe", "w"),
  );
  $process = proc_open("\"" . GIT_HTTP_BACKEND . "\"", $settings, $pipes, null, $env);
  if(is_resource($process))
  {
    fwrite($pipes[0], $php_input);
    fclose($pipes[0]);
    $return_output = stream_get_contents($pipes[1]);
    fclose($pipes[1]);
    $return_code = proc_close($process);
  }

  if(!empty($return_output))
  {
    list($response_headers, $response_body) = $response = preg_split("/\R\R/", $return_output, 2, PREG_SPLIT_NO_EMPTY);
    foreach(preg_split("/\R/", $response_headers) as $response_header)
    {
      header($response_header);
    }

    if(isset($request_headers["Accept-Encoding"]) && strpos($request_headers["Accept-Encoding"], "gzip") !== false && GZIP_SUPPORT)
    {
      $gzipoutput = gzencode($response_body, 6);
      ini_set("zlib.output_compression", "Off");
      header("Content-Encoding: gzip");
      header("Content-Length: " . strlen($gzipoutput));
      echo $gzipoutput;
    }
    else
    {
      echo $response_body;
    }
  }

}
