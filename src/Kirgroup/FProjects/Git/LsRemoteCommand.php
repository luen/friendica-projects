<?php

namespace Kirgroup\FProjects\Git;

use PHPGit\Command;

/**
 * git ls-remote
 *
 * @author Fabio Comuni <fabrix.xm@gmail.com>
 * @author Kazuyuki Hayashi <hayashi@valnur.net>
 */
class LsRemoteCommand extends Command
{

    /**
     *  List references in a remote repository
     *
     * ``` php
     * $git = new PHPGit\Git();
     * $git->setRepository('/path/to/repo');
     * $revcount = $git->lsremote();
     * ```
     *
     * ##### Output Example
     *
     * ``` php
     * [
     *  "HEAD",
     *  "ref/heads/master",
     *  "ref/heads/develop"
     * ]
     * ```
     *
     * ##### Options
     *
     * - **heads** (_boolean_) Limit to only refs/heads
     * - **tags**  (_boolean_) Limit to only refs/tags
     *
     * @param string $remote   [optional] The "remote" repository to query. This parameter can be either a URL or the name of a remote.
     * @param array  $options  [optional] An array of options
     *
     * @return array
     */
    public function __invoke($remote = '', $options = [])
    {
        $builder = $this->git->getProcessBuilder()
            ->add('ls-remote');


        if ($options['heads']===true)
            $builder->add("--heads");

        if ($options['tags']===true)
            $builder->add("--tags");

        if ($remote!='')
            $builder->add($remote);

        $process = $builder->getProcess();

        $output = $this->git->run($process);
        $lines = $this->split($output);

        $result = [];
        foreach($lines as $line) {
            $name = str_replace("refs/heads/", "", trim(explode("\t",$line,2)[1]));
            $ref = trim(explode("\t",$line,2)[0]);
            $result[$name]=[
                'name'=>$name,
                'hash'=>$ref
            ];
        }

        return $result;
    }

}
