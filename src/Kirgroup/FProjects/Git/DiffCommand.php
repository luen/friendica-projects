<?php

namespace Kirgroup\FProjects\Git;

use PHPGit\Command;

/**
 * git diff
 *
 * @author Fabio Comuni <fabrix.xm@gmail.com>
 * @author Kazuyuki Hayashi <hayashi@valnur.net>
 */
class DiffCommand extends Command
{

    /**
     *  Show changes between commits
     *
     * ``` php
     * $git = new PHPGit\Git();
     * $git->setRepository('/path/to/repo');
     * $diff = $git->diff($ref1, $ref2);
     * ```
     *
     *
     * @param string string $ref1
     * @param string string $ref2
     *
     * @return string
     */
    public function __invoke($ref1, $ref2)
    {
        $builder = $this->git->getProcessBuilder()
            ->add('diff')
            ->add($ref1)
            ->add($ref2);

        $process = $builder->getProcess();

        $output = $this->git->run($process);

        return $output;
    }

}
