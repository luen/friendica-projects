{{extends file="./base.tpl"}}

{{block name=content}}
{{$breadcrumb}}
{{if $type==image}}
    <img src="./projects/{{$user}}/{{$slug}}/blob/{{$hash}}">
{{elseif $type==binary}}
    <div class="pjs-download">
        <p><span class="mega-octicon octicon-file-binary"></span></p>
        <p>{{$path|basename}}</p>
        <p><a href="./projects/{{$user}}/{{$slug}}/blob/{{$hash}}">{{"Click here to download"|t}}</a></p>
    </div>
{{elseif $type==markdown}}
    {{$code}}
{{else}}
    <div class="source-code $type">
    {{$code|numerize}}
    </div>
{{/if}}

{{/block}}
