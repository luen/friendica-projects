{{extends file="./base.tpl"}}

{{block name=content}}
<h1><i class="mega-octicon octicon-git-pull-request pjs-status-{{$item.status}}"></i> {{$item.title}}</h1>

{{include file="common_tabs.tpl" tabs=$tabs}}

{{$diff|format_diff}}
{{/block}}

