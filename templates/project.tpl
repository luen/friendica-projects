{{extends file="./base.tpl"}}

{{block name=content}}

{{$breadcrumb}}

{{include "./_actions.tpl"}}

<div class="pjs-commit-detail">
	<img src="{{$lastcommit.details.micro}}">
	<div class="pjs-c-text">{{$lastcommit.title}}</div>
	<div class="pjs-c-meta">
	by {{$lastcommit.name}} {{$lastcommit.date|relative_date}} <a href="./projects/{{$user}}/{{$slug}}/commits/{{$lastcommit.hash}}" class="pjs-ref btn">{{$lastcommit.hash|substr:0:10}}</a>
	</div>
</div>

<div>{{$tree}}</div>

{{if $readme}}
<div class="pjs-readme">
{{$readme}}
</div>
{{/if}}

{{/block}}

{{block name=comments}}
{{$comments_html}}
{{/block}}
