{{extends file="./base.tpl"}}

{{block name=content}}

    <div class="pjs-commit">
        <a class="btn" href="./projects/{{$user}}/{{$slug}}/{{$commit.hash}}">&lt;&gt;</a>
        <a class="pjs-ref btn" href="./projects/{{$user}}/{{$slug}}/commits/{{$commit.hash}}">{{$commit.hash|substr:0:10}}</a>
        <img src="{{$commit.details.micro}}">
        <div class="pjs-c-text">{{$commit.title}}</div>
        <div class="pjs-c-message">{{$commit.message}}</div>
        <div class="pjs-c-meta">{{$commit.name}} on {{$commit.date|date_format:'M d Y'}}</div>
    </div>

    {{foreach $difs as $file=>$dif}}

    <div class="pjs-diff">
        {{$dif|format_diff}}
    </div>

    {{/foreach}}

{{/block}}
