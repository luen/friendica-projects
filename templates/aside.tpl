<h1 class="pjs-image">
    <i class="octicon octicon-{{$repotype}}"></i>
</h1>
<div class="pjs-widget">
{{foreach $links as $link}}
<a href="{{$link.1}}" class="btn {{if $link.3}}active{{/if}}"><i class="octicon octicon-{{$link.2}}"></i> {{$link.0}} {{if !is_null($link.4)}}<span class="pjs-counter">{{$link.4}}</span>{{/if}}</a>
{{/foreach}}
</div>

<div class="pjs-widget">
    {{if $cloneurl}}
    <div class="pjs-clone">

        {{"Clone this project:"|t}} <input type="text" readonly="readonly" value="{{$cloneurl}}" name="clone_url">

    </div>
    {{/if}}
</div>
