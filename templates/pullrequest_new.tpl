
<h2>{{"New pull request"|t}}</h2>
<form method="post" id="pjs-form-new">
    <input type='hidden' name='form_security_token' value='{{$form_security_token}}'>
    <input type='hidden' name='cid' value="{{$contact.id}}">

    {{include file="field_select.tpl" field=$base_fork}}
    {{include file="field_select.tpl" field=$head_fork}}

    {{include file="field_input.tpl" field=$title}}
    {{include file="field_textarea.tpl" field=$description}}

    {{if !is_null($diff)}}
    <div id="pjs_ajax_diff">
    {{$diff|format_diff}}
    </div>
    {{else}}
    <div class="pjs-empty">
        {{"Can't show you the diff at the moment. Sorry"|t}}
    </div>
    {{/if}}

    <div class="settings-submit-wrapper" >
        <input type="submit" name="submit" class="settings-submit" value="{{'Create'|t|escape:'html'}}" />
    </div>
</form>

<script>
var br="{{$base_ref}}";
var hr="{{$head_ref}}";

if ($("#pjs_ajax_diff").length>0) {

    function pj_diff_update() {
        var url="{{$baseurl}}/projects/_ajax_/{{$user}}/{{$slug}}/diff/"+br+"/"+hr;
        $.get(url, function(val) {
            $("#pjs_ajax_diff").html(val);
        });
    }

    $("#id_base_fork").on('change', function(e) {
        br = $(this).val();
        pj_diff_update();
    });

    $("#id_head_fork").on('change', function(e) {
        hr = $(this).val();
        location.href = "{{$baseurl}}/projects/{{$user}}/{{$slug}}/pull/new/" + hr;
    });
}
</script>
