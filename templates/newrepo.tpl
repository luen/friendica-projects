<h2>{{"Create new project"|t}}</h2>
<form method="post" id="pjs-form-new">
    <input type='hidden' name='form_security_token' value='{{$form_security_token}}'>

    {{include file="field_input.tpl" field=$newrepo}}
    {{include file="field_select.tpl" field=$type}}
    {{include file="field_input.tpl" field=$description}}

    <h4>Access</h4>
    <div id="settings-default-perms" class="settings-default-perms" >
        <a href="#profile-jot-acl-wrapper" id="settings-default-perms-menu" class='popupbox'>{{"Visibility"|t}}</a>
        <div id="settings-default-perms-select" style="display: none; margin-bottom: 20px" >
            <div style="display: none;">
                <div id="profile-jot-acl-wrapper" style="width:auto;height:auto;overflow:auto;">
                {{$aclselect}}
                </div>
            </div>
        </div>
    </div>

    {{include file="field_checkbox.tpl" field=$nopost}}

    <div class="settings-submit-wrapper" >
        <input type="submit" name="submit" class="settings-submit" value="{{"Create"|t|escape:'html'}}" />
    </div>
</form>
