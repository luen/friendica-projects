{{extends file="./base.tpl"}}

{{block name=content}}
<h1><i class="mega-octicon octicon-git-pull-request pjs-status-{{$item.status}}"></i> {{$item.title}}</h1>

{{include file="common_tabs.tpl" tabs=$tabs}}

{{if $isowner && $item.status<=1}}
<div id="pjs-pr-actions">
    {{if $item.status==0}}<a href="{{$url}}/merge" id="pjs-pr-merge" class="btn large ok"><span class="octicon octicon-git-merge"></span> Merge</a>{{/if}}
    <a href="{{$url}}/close" id="pjs-pr-close" class="btn large"><span class="octicon octicon-git-merge"></span> Close</a>
</div>
{{/if}}

{{/block}}

{{block name=comments}}

    {{$thread_html}}

{{/block}}
