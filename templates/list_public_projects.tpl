<div class="pjs-home">

    <h1>{{"Public projects on this node"|t}}</h1>

    <div class="pjs-list psj-public-list">
    {{foreach $repos as $user=>$userrepos}}
        <h3><img src="{{$userrepos.0.micro}}"> {{$userrepos.0.username}}</h3>
        {{foreach $userrepos as $repo}}
            {{if $repo.remote}}
            <i class="mega-octicon octicon-repo-clone"></i>
            {{else}}
            <i class="mega-octicon octicon-repo"></i>
            {{/if}}
            <dl>
                <dt><a href="{{$baseurl}}/projects/{{$user}}/{{$repo.name}}">{{$user}}/{{$repo.name}}</a></dt>
                {{if $repo.remote}}<dd class="pjs-fork">forked from <a href='{{$repo.remote.url}}'>{{$repo.remote.name}}</a><dd>{{/if}}
                <dd>{{$repo.description}}</dd>
            </dl>
        {{/foreach}}
    {{/foreach}}
    </div>

</div>

<script>
    $(".pjs-tool.pjs-delete").on("click", function(e){
        e.preventDefault();
        var reponame = $(this).data().repo;
        if ( confirm("{{"Do you want to delete the project '%s'?\\n\\nThis operation cannot be undone."|t}}".replace("%s", reponame)) ){
            $.post(baseurl+"/projects/del", {slug:reponame}, function(data) {
                if (data=="ok"){
                    window.location.reload(true);
                }
            });
        }
    });
</script>
