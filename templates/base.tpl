<div class="pjs-page-wrapper">
{{block name=header}}
<div class="pjs-header">
    <ul class="pjs-info">
        {{if $commits}}<li class="pjs-commits"><i class='octicon octicon-git-commit'></i><a href="./projects/{{$user}}/{{$slug}}/commits/{{$ref}}">{{$commits}} commits</a></li>{{/if}}
        {{if $branches}}<li class="pjs-branches"><i class='octicon octicon-git-branch'></i>{{$branches}} branches</li>{{/if}}
        {{if $branches}}<li class="pjs-like"><i class='octicon octicon-thumbsup'></i>{{$like_count}} like</li>{{/if}}
        {{if $branches}}<li class="pjs-dislike"><i class='octicon octicon-thumbsdown'></i>{{$dislike_count}} dislike</li>{{/if}}
        {{if $forkurl}}
            <li>
                <form method="POST" action="{{$forkurl}}">
                <button type="submit" name="fork"><i class="octicon octicon-repo-forked"></i><br>fork</button>
                </form>
            </li>
        {{/if}}
    </ul>
    <h1 class="pjs-name">
    {{if $remote}}
        {{if $remote=='new'}}
            <i class='mega-octicon octicon-repo'></i>
        {{else}}
            <i class='mega-octicon octicon-repo-clone'></i>
        {{/if}}
    {{/if}}
    <a href="./projects/{{$user}}">{{$user}}</a> / <a href="./projects/{{$user}}/{{$slug}}">{{$slug}}</a>
    {{if $remote}}{{if $remote!='new'}}<div class="pjs-fork">forked from <a href='{{$remote.url}}'>{{$remote.name}}</a></div>{{/if}}{{/if}}
    </h1>


    <div class="pjs-description">{{$description}}</div>
</div>
{{/block}}

{{block name=content}}
{{/block}}
</div>

{{block name=comments}}
 <!-- this block is out of pjs-page-wrapper -->
{{/block}}
