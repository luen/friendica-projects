{{foreach $logs as $date=>$log}}

    <div class="pjs-log-date">Commits on {{$date|date_format:'M d, Y'}}</div>
    <div class="pjs-log-commits">
    {{foreach $log as $commit}}
    <div class="pjs-commit">
        <a class="btn" href="./projects/{{$user}}/{{$slug}}/{{$commit.hash}}">&lt;&gt;</a>
        <a class="pjs-ref btn" href="./projects/{{$user}}/{{$slug}}/commits/{{$commit.hash}}">{{$commit.hash|substr:0:10}}</a>
        <img src="{{$commit.details.micro}}">
        <div class="pjs-c-text">{{$commit.title}}</div>
        <div class="pjs-c-meta">{{$commit.name}} on {{$commit.date|date_format:'M d Y'}}</div>
    </div>
    {{/foreach}}
    </div>
{{/foreach}}
