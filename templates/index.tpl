<div class="pjs-home">

    <h1>{{"Projects"|t}} {{if $landlord}}<a class="pjs-tool pjs-add" href='./projects/new'>{{"add new"|t}}</a>{{/if}}</h1>

    <div class="pjs-list">
    {{foreach $repos as $repo}}
        {{if $repo.remote}}
        <i class="mega-octicon octicon-repo-clone"></i>
        {{else}}
        <i class="mega-octicon octicon-repo"></i>
        {{/if}}
        <dl>
            <dt><a href="./projects/{{$user}}/{{$repo.name}}">{{$repo.name}}</a> {{if $landlord}}<a class="pjs-tool pjs-delete" href='#' data-repo='{{$repo.name}}'>{{"delete"|t}}</a>{{/if}}</dt>
            {{if $repo.remote}}<dd class="pjs-fork">forked from <a href='{{$repo.remote.url}}'>{{$repo.remote.name}}</a><dd>{{/if}}
            <dd>{{$repo.description}}</dd>
        </dl>
    {{/foreach}}
    </div>

</div>

<script>
    $(".pjs-tool.pjs-delete").on("click", function(e){
        e.preventDefault();
        var reponame = $(this).data().repo;
        if ( confirm("{{"Do you want to delete the project '%s'?\\n\\nThis operation cannot be undone."|t}}".replace("%s", reponame)) ){
            $.post(baseurl+"/projects/del", {slug:reponame}, function(data) {
                if (data=="ok"){
                    window.location.reload(true);
                }
            });
        }
    });
</script>
