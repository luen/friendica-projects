<table class="pjs-tree">
{{foreach $tree as $e}}
<tr>
    <td class="psj-tree-{{$e.type}}">
        {{if $e.link}}<a href="{{$e.link}}">{{$e.file}}</a>
        {{else}}{{$e.file}}
        {{/if}}
    </td>
    {{if $e.log}}
    <td>
        {{$e.log.title}}
    </td>
    <td>
        <!-- this is riduculus -->
        {{foreach "("|explode:($e.log.date|relative_date) as $d}}{{$d}}{{break}}{{/foreach}}
    </td>
    {{else}}
    <td></td><td></td>
    {{/if}}
</tr>
{{/foreach}}
</table>
