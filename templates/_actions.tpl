{{if $remote!='new' && $isowner }}
    <div class="pjs-repo-actions">
        <a class="btn" href="./projects/{{$user}}/{{$slug}}/pull/new/{{$ref}}" title="{{'Create new pull request'|t}}"><span class="octicon octicon-git-pull-request"></span> pull request</a>
    </div>
{{/if}}
