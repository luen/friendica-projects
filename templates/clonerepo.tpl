<h2>Fork project</h2>
<form method="post" action="{{$posturl}}" id="pjs-form-clone">
    <input type='hidden' name='form_security_token' value='{{$form_security_token}}'>
    <input type='hidden' name='r' value='{{$r}}'>
    <input type='hidden' name='newrepo' value="{{$newrepo}}">
    <input type='hidden' name='description' value="{{$description}}">
    <input type='hidden' name='type' value="{{$type}}">

    <b>{{$newrepo}}</b> from <tt>{{$remote_url}}</tt>
    <p>{{$description}}</p>

    <h4>Access</h4>
    <div id="settings-default-perms" class="settings-default-perms" >
        <a href="#profile-jot-acl-wrapper" id="settings-default-perms-menu" class='popupbox'>{{"Visibility"|t}}</a>
        <div id="settings-default-perms-select" style="display: none; margin-bottom: 20px" >
            <div style="display: none;">
                <div id="profile-jot-acl-wrapper" style="width:auto;height:auto;overflow:auto;">
                {{$aclselect}}
                </div>
            </div>
        </div>
    </div>

    {{include file="field_checkbox.tpl" field=$nopost}}

    <div class="settings-submit-wrapper" >
        <input type="submit" name="submit" class="settings-submit" value="{{"Create"|t|escape:'html'}}" />
    </div>
</form>
