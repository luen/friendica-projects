<div id="tree_path">
{{$sel_branch}}
 -
{{foreach $breadcrumb as $n=>$l}}
 <a class="{{if $l@first}}pjs-bc-first{{/if}} {{if $l@last}}pjs-bc-last{{/if}}" href="{{$l}}">{{$n}}</a> {{if !$is_file}}/{{else}}{{if !$l@last}}/{{/if}}{{/if}}
{{/foreach}}
</div>
