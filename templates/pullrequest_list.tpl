{{extends file="./base.tpl"}}

{{block name=content}}

{{include file="common_tabs.tpl" tabs=$filters}}

{{if count($list)==0}}
    <div class="pjs-empty">
    {{"No pull requests to show"|t}}
    </div>
{{else}}
    <dl class="pjs_list">
    {{foreach $list as $item }}

        <dt><span class="octicon octicon-git-pull-request pjs-status-{{$item.status}}"></span> <a href="{{$item.plink}}">{{$item.title}}</a></dt>
        <dd>#{{$item.guid|substr:24:8}} {{"opened by"|t}} {{$item['author-name']}} {{$item.created|relative_date}}
            {{if $item.status==2}}
            {{"closed"|t}} {{$item.closed|relative_date}}
            {{/if}}
            {{if $item.status==3}}
            {{"merged"|t}} {{$item.closed|relative_date}}
            {{/if}}
        </dd>

    {{/foreach}}
    </dl>
{{/if}}

{{/block}}

